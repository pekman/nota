# Si no soy Nadie No me Vigilan

« ¿Por qué me vigilan, si no soy nadie? | Marta Peirano | TEDxMadrid »
[Video](https://invidio.us/watch?v=NPE7i8wuupk&local=true)

Si queréis el [enlace de la evidencia de Malte Spitz](https://public.tableau.com/profile/datalicious.pty.ltd#!/vizhome/MalteSpitzCallData/MalteSpitzcalldatadashboard)

Ahora entiendes que no se trata de "solo suposiciones", esta frase es
buena: «La gente a dejado de ser inteligente porque sus teléfonos lo
son».

También tienes una buena referencia
[aquí](https://lablibre.tuxfamily.org/richard-stallman-en-retina-version-corta/)


Toma acción xD!!
