## NPM in Python virtualenv

### Pre-requisites

    sudo pacman -S python-virtualenv

### Make virtualenv for python

    virtualenv -p python3 venv

    source venv/bin/activate

    pip install nodeenv

### NPM on vitualenv

    nodeenv nodejsenv

    source nodejsenv/bin/activate

or from source version LTS (recomend)

    nodeenv --node=14.15.4 --source --jobs=4 nodejs

    source nodejs/bin/activate

### Test app NPM on virtualenv

    node -v

    npm -v

    npm install -g gulp

### Deactivate environment NodeJS

    deactivate_node

### Deactivate environment Python

    deactivate

### Delete all (NPM and Python) virtualenvs

    rm -rv .venv/ .nodejsenv/

## Usage

This assumes that you have already created your virtualenvs

### Active Python (1)

```
    source venv/bin/activate
```

or in Hyperbash

```
    activate venv/
```

### Active NPM (2)

```
    source nodejsenv/bin/activate
```

or in Hyperbash

```
    activate nodejsenv/
```

### Update packages

    npm update --save/--save-dev -f
