# Cifrar Strings Manualmente

Aquí explico cómo cifrar texto, para descifrar en una web y tambien
recomiendo programas web para pasarse documentos...

# Cifrar String

    $ echo -n 'Hola este es el mensaje secreto' | base64

# Descifrar String en Terminal

    $ echo 'SG9sYSBlc3RlIGVzIGVsIG1lbnNhamUgc2VjcmV0bw==' | base64 --decode
     -> Hola este es el mensaje secreto

# Descifrar Online, Pasar Fotos y Pasar Archivos

o/ puedes descifrar strings base64
[aquí](https://www.base64decode.org/), tal vez tambien te sea de
ayuda, [framapic.org](https://framapic.org/) para subir fotos, o
[uguu.se](https://uguu.se/), puede servir para pasar archivos
anónimamente también xD!!, es software libre, porque las webs brindan
el código fuente que están usando.

y antes que me olvide tambien puede ser necesario, para enviar texto
plano [paste.debian.net](https://paste.debian.net/)
