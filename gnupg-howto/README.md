# Ejemplo de sifrado sin pinentry daemon

Esto es cuando no funciona bien el i3wm, cuando el lxsession no está
bien empaquetado en la distro, causa problemas al encenderlo

`gpg --batch --passphrase _¿3MaiL50 --symmetric file.zip`

- Duplicará el mismo archivo sólo que cifrado con la extención .gpg si
  eliminas el de la extención con únicamente .zip, sólo se podrá
  descifrar el .gpg (para obtener el archivo original) con el
  siguiente comando:

`gpg --batch --passphrase _¿3MaiL50 --output el-file.zip --decrypt file.zip.gpg`

- ^ es el comando que descifrará los mensajes que me envíen por correo
  fácilmente identificables con la extención gpg
