# PUBLICAR UNA URL PERSONALIZADA EN EL WIFI DE TU PROPIA CASA xD!!

## deber hacer un URL virtual, al estilo

# `saravia.local.org`

- y agregarlo a hosts /etc/hosts, ejemplo:
- `/etc/hosts: static lookup table for host names`

        #<ip-address>	<hostname.domain.org>	<hostname>
        127.0.0.1       localhost.localdomain   localhost localhost
        ::1		localhost.localdomain	localhost

        # virtualhost
        127.0.0.1       localhost.localdomain   localhost localhost

        # My hosts
        127.0.0.1       saravia.local.org

        # End or file

- y luego apuntar esa URL desde el servidor http, ejemplo en Apache:

        # saravia
        <VirtualHost *:80>
            DocumentRoot "/srv/http/www/h-node-store/public/"
            ServerName saravia.local.org
            ErrorLog "/var/log/httpd/saravia.local.org-error_log"
            CustomLog "/var/log/httpd/saravia.local.org-access_log" common
            <Directory "/srv/http/www/tu-directorio-del-proyecto">
                Options Indexes FollowSymLinks
                AllowOverride all
                Order allow,deny
                Allow from all
            </Directory>
        </VirtualHost>

- luego reiniciar el servidor web
- ** código anterior debe ser colocado en `/etc/httpd/conf/extra/httpd-vhosts.conf` **

- consulta y no se debe activar nada en apache? ✓
- Una consulta personalizada cuesta unos 5 PEN ×D
- LOL
- > consulta y no se debe activar nada en apache?
- asegurase que este descomentado `httpd-vhost.conf` en `/etc/httpd/conf/httpd.conf`
    * ejemplo:

            # Virtual hosts
            Include conf/extra/httpd-vhosts.conf
