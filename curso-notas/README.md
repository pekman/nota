# Aratacademy

## Episodio 198

## Cómo sacar el máximo provecho a un curso virtual

Simula una clase con la misma seriedad de como con un profesor que
respetas y admiras en un aula presencial.

### Toma en cuenta y Enriquece el proceso

- Toma apuntes con lápiz en un cuaderno en físico
- Usar la computadora si tuvieras una pantalla sobria, sin
  distracciones gráficas.
- Si usas la computadora, deshabilita las notificaciones emergentes o
  sonoras como de correos, mensajes instantáneos, etc.
- Una gran ventaja informática radica en el control de poder
  desconectar el internet mientras se toma el curso.
- Haz apuntes en formato de mapas mentales, palabras clave, véase Episodio
  189.
- Si tienes un móvil ponlo en modo avión en el horario del curso.

### Traza tus metas y Aclara tus razones

- ¿por qué?, ¿qué? y ¿cómo?.
- Cuanto dura la clase, en que horarios los vas a llevar en cuantas
  horas.
- Fecha de fin y fecha de inicio.
- Cuales son exactamente las habilidades que quiero lograr.
- Cual es el motivo de llevar la clase.
- En cuando tiempo quieres acabar el curso.
- En que días lo vas a llevar.
- Cuantas horas al día.
- El motivo de aprender el curso es por mera curiosidad, por una
  necesidad práctica de corto plazo, por una necesidad práctica de
  largo plazo o una necesidad del trabajo.
- Necesitas acabar el curso lo más rápido posible o absorber realmente
  los conocimientos que desarrolla el curso.
- Cuales son los puntos que exactamente quieres aprender.

### Esencial para tus metas

- Que piezas conforman estos temas.
- Que clases enseñan estas piezas.
- Que conocimientos puedes enfatizar.
- Que conocimientos puedes excluir.
- Que clases puedes ver al doble de velocidad sin perjudicar tus objetivos.

### Como vas a aprender

- Cual es el mejor método.
- Practicando alguna habilidad.
- Cómo vas a practicar.
- Resolviendo exámenes académicos.
- Qué exámenes.

# continuará
