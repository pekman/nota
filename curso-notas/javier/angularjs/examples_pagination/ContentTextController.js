app.controller('ContentTextController', ['$scope', '$http', '$routeParams', '$q', '$location', 'PingService', function($scope, $http, $routeParams, $q, $location, PingService) {

    $scope.pageSize = 20;
    $scope.currentPage = 0;
    $scope.TotalRows = 0;
    $scope.contentName = '';

    $scope.numberOfPages = function() {
        return Math.ceil($scope.TotalRows / $scope.pageSize);
    };

    $scope.ContentGetCount = function() {

        $('#divEmptyResults').hide();
        ShowLoading();

        var Obj = {
            ContentName: $scope.contentName
        };

        $http.post('API/content/content_get_count/', Obj)
             .success(function(data) {
                 $scope.contentList = [];
                 $scope.TotalRows = parseInt(data, 10);
                 if (parseInt(data, 10) > 0) {
                     HideLoading();
                     $scope.ContentGetPaged(0);
                 } else {
                     HideLoading();
                     $('#divEmptyResults').show();
                 }
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
                 return;
             });
    };

    $scope.ContentGetPaged = function(Page) {

        $scope.currentPage = Page;

        var Obj = {
            ContentName: $scope.contentName,
            StartingRow: Page * $scope.pageSize
        };

        ShowLoading();
        $http.post('API/content/content_get_paged/', Obj)
             .success(function(data) {
                 HideLoading();
                 $scope.contentList = data;
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });
    };

    $scope.UpdateCanassign = function(contentid) {

        var Objdata = {
            ContentID: contentid,
            CanAssign: $("#chkCanAssign" + contentid).is(":checked")
        };

        ShowLoading();

        $http.post('API/content/content_update_canassign/', Objdata)
             .success(function(res) {
                 alert('Estado Cambiado.');
                 HideLoading();
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });
    };

    if($location.path() === '/texts-admin'){
        $scope.ContentGetCount($scope.contentName);
    }

    $scope.refreshCancel = function(rowform) {
        $scope.ContentGetPaged($scope.currentPage);
        rowform.$cancel();
    };

    $scope.checkName = function(contentname, id) {

        if (typeof(contentname) === 'undefined' || contentname === '') {
            return 'Debe ingresar un nombre válido';
        } else {

            if (typeof(id) === 'undefined') {
                id = 0;
            }

            var validator = {
                ID: id,
                Name: contentname,
                Type: 'Content'
            };

            var d = $q.defer();

            $http.post('API/globalmethods/validatename/', validator)
                 .success(function(res) {

                     console.log(res);

                     if (parseInt(res[0]) === 0) {
                         d.resolve();
                     } else {
                         d.resolve('El nombre ' + contentname + ' ya existe');
                     }

                 }).error(function(e) {
                     d.reject('Server error!');
                 });

            return d.promise;

        }
    };

    $scope.getContent = function() {

        var Obtainer = {
            ContentID: $routeParams.ContentID
        };

        ShowLoading();
        $http.post('API/content/content_get/', Obtainer)
             .success(function(data) {
                 $scope.content = data;
                 HideLoading();
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });
    };

    if($location.path() === '/text-edit/'+$routeParams.ContentID+''){
        $scope.getContent();
    }

    $scope.cancel = function() {
        window.location.href = '#/texts-admin';
    };

    $scope.saveContent = function(content) {

        if (!$('#txtContentName').val()) {
            $('#txtContentName').focus();
            alert('El contenido necesita un nombre');
            return;
        }

        if(!$('#txtcontent').val()){
            $('#txtcontent').focus();
            alert('Necesita un contenido antes de guardar');
            return;
        }

        if($routeParams.ContentID > 0) {

            var Updater = {
                ContentID: $routeParams.ContentID,
                ContentName: content.ContentName,
                content: content.content,
                CanAssign: $("#chkCanAssign").is(":checked")
            };

            ShowLoading();
            $http.post('API/content/content_update/', Updater)
                 .success(function(res) {
                     alert('Contenido Modificado.');
                     window.location.href = '#/texts-admin';
                     HideLoading();
                 })
                 .error(function(err) {
                     HideLoading();
                     console.log(err);
                     alert(err);
                 });

        } else {

            var Inserter = {
                ContentName: content.ContentName,
                content: content.content,
                CanAssign: $("#chkCanAssign").is(":checked")
            };

            ShowLoading();
            $http.post('API/content/content_insert/', Inserter)
                 .success(function(res) {
                     alert('Contenido Modificado.');
                     window.location.href = '#/texts-admin';
                     HideLoading();
                 })
                 .error(function(err) {
                     HideLoading();
                     console.log(err);
                     alert(err);
                 });

        }

    };

}]);
