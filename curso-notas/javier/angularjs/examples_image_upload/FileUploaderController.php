<?php


class FileUploaderController{

    function uploadImg() {

		global $app;
		$data = json_decode($app->request->getBody());

		$data->img1Name = explode(".", $data->img1Name);
		$data->img2Name = explode(".", $data->img2Name);
		$data->img3Name = explode(".", $data->img3Name);
		$data->img4Name = explode(".", $data->img4Name);
		$data->imgSeoName = explode(".", $data->imgSeoName);

		$imgBaseName = sanitize_img_name($data->name);

		if (isset($data->img1)) {
			base64_to_img($data->img1, $imgBaseName.'-1.'.$data->img1Name[1]);
		}

		if (isset($data->img2)) {
			base64_to_img($data->img2, $imgBaseName.'-2.'.$data->img2Name[1]);
		}

		if (isset($data->img3)) {
			base64_to_img($data->img3, $imgBaseName.'-3.'.$data->img3Name[1]);
		}

		if (isset($data->img4)) {
			base64_to_img($data->img4, $imgBaseName.'-4.'.$data->img4Name[1]);
		}

		if (isset($data->imgSeo)) {
			base64_to_img($data->imgSeo, $imgBaseName.'-seo.'.$data->imgSeoName[1]);
		}

		return json_encode('ok');
    }

    function uploadImgZoom() {

		global $app;
		$data = json_decode($app->request->getBody());

		$data->imgLarge1Name = explode(".", $data->imgLarge1Name);
		$data->imgLarge2Name = explode(".", $data->imgLarge2Name);
		$data->imgLarge3Name = explode(".", $data->imgLarge3Name);
		$data->imgLarge4Name = explode(".", $data->imgLarge4Name);

		$imgBaseName = sanitize_img_name($data->name);

		if (isset($data->imgLarge1)) {
			base64_to_img($data->imgLarge1, $imgBaseName.'-1-large.'.$data->imgLarge1Name[1]);
		}

		if (isset($data->imgLarge2)) {
			base64_to_img($data->imgLarge2, $imgBaseName.'-2-large.'.$data->imgLarge2Name[1]);
		}

		if (isset($data->imgLarge3)) {
			base64_to_img($data->imgLarge3, $imgBaseName.'-3-large.'.$data->imgLarge3Name[1]);
		}

		if (isset($data->imgLarge4)) {
			base64_to_img($data->imgLarge4, $imgBaseName.'-4-large.'.$data->imgLarge4Name[1]);
		}

		return json_encode('ok');
    }

    function uploadCategoryImages() {

		global $app;
		$data = json_decode($app->request->getBody());

		$data->img1Name = explode(".", $data->img1Name);
                $data->img2Name = explode(".", $data->img2Name);

                $data->img3Name = explode(".", $data->img3Name);
                $data->img4Name = explode(".", $data->img4Name);
                $data->img5Name = explode(".", $data->img5Name);


		$imgBaseName = sanitize_img_name($data->name);

		if (isset($data->img1)) {
			base64_to_img($data->img1, $imgBaseName.'-social.'.$data->img1Name[1]);
		}
                if (isset($data->img2)) {
			base64_to_img($data->img2, $imgBaseName.'-1.'.$data->img2Name[1]);
		}

                if (isset($data->img3)) {
			base64_to_img($data->img3, $imgBaseName.'-2.'.$data->img3Name[1]);
		}
                if (isset($data->img4)) {
			base64_to_img($data->img4, $imgBaseName.'-3.'.$data->img4Name[1]);
		}
                if (isset($data->img5)) {
			base64_to_img($data->img5, $imgBaseName.'-4.'.$data->img5Name[1]);
		}


		return json_encode('ok');
    }

}
