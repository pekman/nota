app.controller('ProductController', ['$scope', '$http', '$routeParams', '$q', '$location', 'PingService', function($scope, $http, $routeParams, $q, $location, PingService){

    $scope.variantId = 0;

    $scope.productSelected = {};
    $scope.product = {};
    $scope.brands = {};
    $scope.sizes = {};
    $scope.variant = {};
    $scope.variants = [];
    $scope.isnewsize = false;
    $scope.contents = {};
    $scope.content = {};
    $scope.content.content = {};

    $scope.toUpdate = false;

    $scope.formVariant = {};
    $scope.formVariant.descriptionSeo = '';
    $scope.formVariant.name = '';
    $scope.formVariant.sku = '';
    $scope.formVariant.description = '';
    $scope.formVariant.descriptionSeo = '';
    $scope.formVariant.Price = '';
    $scope.formVariant.keywords = '';

    $scope.imgObj = {};
    $scope.imgObj.img1 = false;
    $scope.imgObj.img2 = false;
    $scope.imgObj.img3 = false;
    $scope.imgObj.img4 = false;
    $scope.imgObj.imgSeo = false;
    $scope.ProductShippingInfo = {};
    $scope.ProductBasicShippingInfo = {};
    $scope.ProductName = '';
    $scope.ProductExtraInfo = {};


    $scope.imgLargeObj = {};
    $scope.imgLargeObj.imgLarge1 = false;
    $scope.imgLargeObj.imgLarge2 = false;
    $scope.imgLargeObj.imgLarge3 = false;
    $scope.imgLargeObj.imgLarge4 = false;

    $scope.pageSize = 20;
    $scope.currentPage = 0;
    $scope.TotalRows = 0;

    $scope.colorobj = {};

    $scope.getVariantColorAddVariant = function (variant) {

        if(variant.Color === '' || variant.Color === null || typeof(variant.Color) === 'undefined'){
           $("#selectectColorAddVaraint").spectrum({
                color: "#fff"
            });
        } else {

            $("#selectectColorAddVaraint").spectrum({
                color: variant.Color
            });
        }
        // para salvar: $("#selectectColor").spectrum('get').toHexString();

    };

    $scope.colorInitForEditVariant = function (variantObj) {
        variantObj.ColorName = '';
        $scope.variantpu = variantObj;
        var results = [];
        var searchField = "ColorValue";
        var searchVal = variantObj.Color;
        for (var i=0 ; i < $scope.colorList.length ; i++)
        {
            if ($scope.colorList[i][searchField] === searchVal) {
                results.push($scope.colorList[i]);
                $scope.variantpu.ColorName = results[0].ColorName;
            }
            if($scope.variantpu.ColorName === '' ||
               typeof $scope.variantpu.ColorName === 'undefined'
              ) {
                $scope.ddlcolormode = '0';
            } else {
                $scope.ddlcolormode = '1';
            }

        }

        if (typeof $scope.variantpu.ColorValue === 'undefined' &&
            $scope.variantpu.Color === null
           ) {
            $scope.ddlcolormode = '1';
        }
        if(variantObj.Color === '' ||
           variantObj.Color === null ||
           typeof variantObj.Color === 'undefined') {
            $scope.ddlcolormode = '1';
        }
        $scope.getVariantColorAddVariant(variantObj);

    };

    $scope.disabledFields = function (dis) {
        $('#productName').prop('disabled', dis);
        $('#productBrand').prop('disabled', dis);
        if (!dis) {
            $scope.toUpdate = true;
        }
    };

    //validate if section is "NEW" or "EDIT"
    if (typeof($routeParams.productID) !== 'undefined') {
        if ($routeParams.productID > 0) {
            $scope.label = "Editar";
            $scope.isnew = false;
            $scope.prodId = $routeParams.productID;
            $scope.disabledFields(true);
            $scope.newProductInsert = false;
            $scope.default = 0;
        }
    } else {
        $scope.label = "Nuevo";
        $scope.isnew = true;
        $scope.prodId = 0;
        $scope.newProductInsert = true;
        $scope.default = 1;
    }

    if (
        $location.path() !== '/product-admin'           &&
        $location.path().indexOf('/product-sizes') < 0
    ) {
        $scope.brands = {};
        //Get all brands
        ShowLoading();
        $http.get('API/brands/getbrands/')
             .success(function(data) {
                 HideLoading();
                 $scope.brands = data;
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });
    }

    if ($location.path().indexOf('/product-admin') >= 0 ) {

        $scope.contents = {};
        //Get all brands
        ShowLoading();
        $http.get('API/content/getall/1')
             .success(function(data) {
                 HideLoading();
                 $scope.contents = data;
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });
    }

    $scope.getVariants = function() {
        $scope.sizes = {};
        $scope.variant = {};

        ShowLoading();
        $http.get('API/product/getvariantbyid/' + $routeParams.variantID)
             .success(function(data) {
                 HideLoading();
                 $scope.variant = data;
                 $scope.label = data.Name;
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });

        ShowLoading();
        $http.get('API/product/product_getvariantsizes_admin/' + $routeParams.productID + '/' + $routeParams.variantID)
             .success(function(data) {
                 HideLoading();
                 $scope.variants = data;
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });

    };

    if ($location.path().indexOf('/product-sizes') >= 0 ) {
        $scope.getVariants();
    }


    //It load product and variant data when it is editing
    if (!$scope.isnew && $location.path().indexOf('/product-sizes') < 0) {
        var d = $q.defer();
        $scope.product = {};
        //Get Product information by ID
        ShowLoading();
        $http.get('API/product/getproductbyid/' + $scope.prodId)
             .success(function(data) {
                 HideLoading();
                 $scope.product = data[0];
                 $scope.product.Brand = {"BrandID": $scope.product.BrandID, "Brand": $scope.product.Brand};
                 d.resolve(true);
             })
             .error(function(err) {
                 HideLoading();
                 d.resolve(false);
                 console.log(err);
                 alert(err);
             });

        d.promise.then(function(validproduct) {
            if (validproduct) {
                //$("#productBrand").val($scope.product.Brand);
                // console.log( $("#productBrand"));
                //$("#productBrand").val($scope.product.BrandID);
                //Get all variants from product by ID
                $scope.getVariantsByProductId();
            }
        });
    }


    $scope.CommentsManagerGetAllCount = function(){

        $scope.currentSearch = $("#productName").val();
        ShowLoading();
        $http.get('API/product/products_manager_getcountbysearch/' + $("#productName").val())
             .success(function(data) {
                 HideLoading();
                 $scope.productList = [];
                 $scope.TotalRows = parseInt(data,10);
                 if(parseInt(data,10)>0){
                     $scope.lblMessage = '';

                     $scope.searchProductByName(0);
                 }
                 else{
                     $scope.lblMessage = 'No existen registros para mostrar';
                 }
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });
    };

    $scope.numberOfPages = function()
    {
        return Math.ceil($scope.TotalRows / $scope.pageSize);
    };


    //Function to Search Products
    $scope.searchProductByName = function(Page){

        $scope.currentPage=Page;
        ShowLoading();
        $http.get('API/product/searchbyname/' + Page * $scope.pageSize + '/' + $scope.currentSearch)
             .success(function(data) {
                 HideLoading();
                 $scope.productList = data;
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });
    };

    //Function to find all variants from product
    $scope.getVariantsByProductId = function(){
        ShowLoading();
        $http.get('API/product/getvariantsbyproductid/' + $routeParams.productID)
             .success(function(data) {
                 HideLoading();
                 $scope.variants = data;
             })
             .error(function(err) {
                 HideLoading();
                 alert(err);
             });
    };

    //Function modalVariant
    $scope.modalVariant = function(){
        //Validate Product input
        if (typeof($scope.product.Name)=="undefined" || $scope.product.Name === "") {
            alert("Ingresar el nombre");
            return;
        }
        //Validate BrandID input
        if (typeof($scope.product.Brand)=="undefined" || $scope.product.Brand === "") {
            alert("Escoger la marca");
            return;
        }
        $scope.variantId = 0;
        var d = $q.defer();
        if ($scope.isnew) {
            //Validate product name
            ShowLoading();
            $http.get('API/product/validateproductname/' + $scope.product.Name)
                 .success(function(data) {
                     if (parseInt(data) === 1) {
                         HideLoading();
                         alert("El producto ya existe");
                         d.resolve(false);
                     } else if (parseInt(data) === 0) {
                         //insert new product and save ID
                         ShowLoading();
                         $http.get('API/product/insertproduct/' + $scope.product.Name + '/' + $scope.product.Brand)
                              .success(function(data) {
                                  HideLoading();
                                  $scope.prodId = data;
                                  $scope.isnew = false;
                                  d.resolve(true);
                              })
                              .error(function(err) {
                                  HideLoading();
                                  console.log(err);
                                  alert(err);
                                  d.resolve(false);
                              });
                     } else if (data === "error") {
                         HideLoading();
                         alert("Ha ocurrido un error");
                         d.resolve(false);
                     }
                 })
                 .error(function(err) {
                     HideLoading();
                     console.log(err);
                     alert(err);
                 });
        } else {
            HideLoading();
            d.resolve(true);
        }

        d.promise.then(function(validname) {
            if (validname) {
                HideLoading();
                //Disabled Fields
                $scope.disabledFields(true);

                $("#img1Name").val('');
                $("#img2Name").val('');
                $("#img3Name").val('');
                $("#img4Name").val('');
                $("#imgSeoName").val('');
                $scope.formVariant = {};

                //Open Layout
                var options = 'toggle';
                $('#myModal').modal(options);
                // for not select by default at new product
                $scope.emptychild = {};
                $scope.emptychild.Color = '';
                $scope.colorInitForEditVariant($scope.emptychild);
            }
        });

    };

    //Function to AddVariants
    $scope.addVariant = function(){

        var color = '';

        if($scope.ddlcolormode === '1') {
            color = $scope.variantpu.Color ? $scope.variantpu.Color : null;
        }
        else {
            color = $("#selectectColorAddVaraint").spectrum('get').toHexString();
        }

        //Validate Variant
        if ( $scope.variantForm.$invalid ){
            alert("Existen campos con error. \nRevisar el formulario.");
            return;
        }
        if ( typeof($scope.formVariant.description) === 'undefined' || (($scope.formVariant.description).trim()).length < 10) {
            alert("Debe ingresar una descripción de mayor tamaño");
            return;
        }
        if ( typeof($scope.formVariant.descriptionSeo) === 'undefined' || (($scope.formVariant.descriptionSeo).trim()).length < 10) {
            alert("Debe ingresar una descripción SEO de mayor tamaño");
            return;
        }
        if(color == null) {
            alert('Seleccione un color');
            return;
        }
        //validar variant name
        if($scope.IsDimensionEnabled === true) {
            if($('#txtHeight').val() === '' ||
               $('#txtHeight').val() === null ||
               typeof $('#txtHeight').val() === 'undefined') {
                $('#txtHeight').focus();
                alert('Un alto es requerido ');
                return;
            }

            if($('#txtLength').val() === '' ||
               $('#txtLength').val() === null ||
               typeof $('#txtLength').val() === 'undefined') {
                $('#txtLength').focus();
                alert('Un largo requerido');
                return;
            }

            if($('#txtWidth').val() === '' ||
               $('#txtWidth').val() === null ||
               typeof $('#txtWidth').val() === 'undefined') {
                $('#txtWidth').focus();
                alert('Un ancho es requerido');
                return;
            }

            if($('#txtWeight').val() === '' ||
               $('#txtWeight').val() === null ||
               typeof $('#txtWeight').val() === 'undefined') {
                $('#txtWeight').focus();
                alert('Un peso es requerido');
                return;
            }

            if(($('#txtHeight').val() === parseFloat($('#txtHeight').val(),10)) || (parseFloat($('#txtHeight').val(),10) <= 0) ) {
                alert('Debe ingresar un alto valido');
                $('#txtHeight').focus();
                return;
            }

            if(($('#txtLength').val() === parseFloat($('#txtLength').val(),10)) || (parseFloat($('#txtLength').val(),10) <= 0) ) {
                alert('Debe ingresar un largo valido');
                $('#txtLength').focus();
                return;
            }

            if(($('#txtWidth').val() === parseFloat($('#txtWidth').val(),10)) || (parseFloat($('#txtWidth').val(),10) <= 0) ) {
                alert('Debe ingresar un ancho valido');
                $('#txtWidth').focus();
                return;
            }

            if(($('#txtWeight').val() === parseFloat($('#txtWeight').val(),10)) || (parseFloat($('#txtWeight').val(),10) <= 0) ) {
                alert('Debe ingresar un peso valido');
                $('#txtWeight').focus();
                return;
            }
        }

        var d = $q.defer();

        //Validate product name
        ShowLoading();
        $http.get('API/product/validatevariantname/' + $scope.product.Name + ' ' + $scope.formVariant.name + '/' + $scope.prodId + '/' + $scope.variantId)
             .success(function(data) {
                 HideLoading();
                 if (parseInt(data) === 1) {
                     alert("El producto ya existe");
                     d.resolve(false);
                 } else if (parseInt(data) === 0) {
                     d.resolve(true);
                 } else {
                     alert("Ha ocurrido un error");
                     d.resolve(false);
                 }
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });

        d.promise.then(function(validname) {
            HideLoading();
            //Save Img
            var imgs = {
                id : 0,
                name : $scope.product.Name + ' ' + $scope.formVariant.name,
                img1Name : $("#img1Name").val(),
                img2Name : $("#img2Name").val(),
                img3Name : $("#img3Name").val(),
                img4Name : $("#img4Name").val(),
                imgSeoName : $("#imgSeoName").val(),
                img1 : $scope.formVariant.img1,
                img2 : $scope.formVariant.img2,
                img3 : $scope.formVariant.img3,
                img4 : $scope.formVariant.img4,
                imgSeo : $scope.formVariant.imgSeo
            };

            if (validname === true) {
                ShowLoading();
                $http.post('API/uploader/imguploader/', imgs)
                     .success(function(data) {
                         //Save Variant
                         var variant = {
                             pid  : $scope.prodId,
                             vid  : $scope.variantId,
                             name : $scope.product.Name + ' ' + $scope.formVariant.name,
                             sku  : $scope.formVariant.sku,
                             desc : $scope.formVariant.description,
                             descSeo : $scope.formVariant.descriptionSeo,
                             price: $scope.formVariant.Price,
                             keywords: $scope.formVariant.keywords ? $scope.formVariant.keywords : '',
                             default: $scope.default,
                             color: color,
                             IsDimensionEnabled: $scope.IsDimensionEnabled,
                             Height : $('#txtHeight').val(),
                             Length : $("#txtLength").val(),
                             Weight : $("#txtWeight").val(),
                             Width : $("#txtWidth").val()
                         };

                         $http.post('API/product/insertvariant/', variant)
                              .success(function(data) {
                                  HideLoading();
                                  if (parseInt(data) > 0) {
                                      //Close Modal
                                      var options = 'hide';
                                      $('#myModal').modal(options);
                                      $('body').removeClass('modal-open');
                                      $('.modal-backdrop').remove();
                                      alert("Item guardado");
                                      if ($location.path() == '/product-edit/'+$scope.prodId+'') {
                                          //Refresh Variants List
                                          $scope.getVariantsByProductId();
                                          //Clean Form
                                          $("#img1Name").val('');
                                          $("#img2Name").val('');
                                          $("#img3Name").val('');
                                          $("#img4Name").val('');
                                          $("#imgSeoName").val('');
                                          $scope.formVariant = {};
                                      } else {
                                          $location.path('/product-edit/'+$scope.prodId).replace();
                                      }
                                  } else {
                                      alert("Ocurrio un error inesperado!");
                                  }
                              })
                              .error(function(err) {
                                  HideLoading();
                                  console.log(err);
                                  alert(err);
                                  return;
                              });
                     })
                     .error(function(err) {
                         HideLoading();
                         console.log(err);
                         alert(err);
                         return;
                     });
            } else {
                HideLoading();
                return;
            }
        });

    };

    //Function to SetVariant as DEFAULT
    $scope.setDefault = function($variantId){

        var d = $q.defer();
        //Set Variant as default
        ShowLoading();
        $http.get('API/product/setvariantdefault/' + $scope.prodId +"/"+ $variantId)
             .success(function(data) {
                 HideLoading();
                 if (data) {
                     d.resolve(true);
                 } else {
                     d.resolve(false);
                 }
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });

        d.promise.then(function(updated) {
            if (updated) {
                alert('Se ha actualizado correctamente');
                //Reload variants list
                ShowLoading();
                $http.get('API/product/getvariantsbyproductid/' + $scope.prodId)
                     .success(function(data) {
                         HideLoading();
                         $scope.variants = data;
                     })
                     .error(function(err) {
                         HideLoading();
                         console.log(err);
                         alert(err);
                     });
            } else {
                HideLoading();
                alert('Error al actualizar');
            }
        });
    };

    $scope.loadVariantDetails = function ($variantId){

        $scope.variantId = $variantId;

        $("#preview-1").attr('style', 'display:none');
        $("#preview-2").attr('style', 'display:none');
        $("#preview-3").attr('style', 'display:none');
        $("#preview-4").attr('style', 'display:none');
        $("#preview-seo").attr('style', 'display:none');
        $("#preview-1-img").attr('src', '');
        $("#preview-2-img").attr('src', '');
        $("#preview-3-img").attr('src', '');
        $("#preview-4-img").attr('src', '');
        $("#preview-seo-img").attr('src', '');

        //Get Variant data
        ShowLoading();
        $http.get('API/product/getvariantbyid/' + $variantId)
             .success(function(data) {
                 HideLoading();
                 if (data) {
                     var options = 'toggle';
                     $('#myModal').modal(options);
                     $scope.colorInitForEditVariant(data);
                     $("#variantName").val((data.Name).replace($scope.product.Name, ""));
                     $("#variantName").trigger("input");

                     $("#variantSKU").val(data.Code);
                     $("#variantSKU").trigger("input");
                     $scope.formVariant.description = data.Description;
                     $("#varianDescSeo").val(data.DescriptionSEO);
                     $("#varianDescSeo").trigger("input");

                     if (data.ImageFileName1 !== null && data.ImageFileName1 !== '') {
                         $scope.ImageFileName1 = data.ImageFileName1;
                         $("#img1Name").val((data.ImageFileName1).replace("images_product/", ""));
                         $scope.imgObj.img1 = true;
                     }

                     if (data.ImageFileName2 !== null && data.ImageFileName2 !== '') {
                         $scope.ImageFileName2 = data.ImageFileName2;
                         $("#img2Name").val((data.ImageFileName2).replace("images_product/", ""));
                         $scope.imgObj.img2 = true;
                     }

                     if (data.ImageFileName3 !== null && data.ImageFileName3 !== '') {
                         $scope.ImageFileName3 = data.ImageFileName3;
                         $("#img3Name").val((data.ImageFileName3).replace("images_product/", ""));
                         $scope.imgObj.img3 = true;
                     }

                     if (data.ImageFileName4 !== null && data.ImageFileName4 !== '') {
                         $scope.ImageFileName4 = data.ImageFileName4;
                         $("#img4Name").val((data.ImageFileName4).replace("images_product/", ""));
                         $scope.imgObj.img4 = true;
                     }

                     if (data.SocialImage !== null && data.SocialImage !== '') {
                         $scope.SocialImage = data.SocialImage;
                         $("#imgSeoName").val((data.SocialImage).replace("images_product/", ""));
                         $scope.imgObj.imgSeo = true;
                     }

                     $scope.baseURL = data.BASEURL;

                     $("#variantPrice").val(data.Price);
                     $("#variantPrice").trigger("input");

                     $("#variantKeywords").val(data.Keywords);
                     $("#variantKeywords").trigger("input");

                 } else {
                     alert("Error al recuperar la data");
                 }
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });
    };

    $scope.updateProductBase = function () {
        //Validate Product input
        if (typeof($scope.product.Name)=="undefined" || $scope.product.Name === "") {
            alert("Ingresar el nombre");
            return;
        }

        //Validate BrandID input
        if (typeof($scope.product.Brand)=="undefined" || $scope.product.Brand === "") {
            alert("Escoger la marca");
            return;
        }

        var brandselected = 0;

        if (typeof($scope.product.Brand)!="undefined" && $scope.product.Brand === parseInt($scope.product.Brand, 10)) {
            brandselected = $scope.product.Brand;
        }
        else
            brandselected = $scope.product.Brand.BrandID;

        var d = $q.defer();
        if (!$scope.isnew) {
            //Validate product name
            ShowLoading();
            $http.get('API/product/validateproductname/' + $scope.product.Name + $scope.prodId)
                 .success(function(data) {
                     if (parseInt(data) === 1) {
                         HideLoading();
                         alert("El producto ya existe");
                         d.resolve(false);
                     } else if (parseInt(data) === 0) {
                         //insert new product and save ID
                         $http.get('API/product/updateproduct/' + $scope.prodId + '/' + $scope.product.Name + '/' + brandselected)
                              .success(function(data) {
                                  HideLoading();
                                  if (data === 1) {
                                      alert("Se actualizó correctamente.");
                                  } else if (data === 0){
                                      alert("Error al actualizar Producto.");
                                  }
                                  $scope.isnew = false;
                                  d.resolve(true);
                              })
                              .error(function(err) {
                                  HideLoading();
                                  console.log(err);
                                  alert(err);
                                  d.resolve(false);
                              });
                     } else if (data === "error") {
                         HideLoading();
                         alert("Ha ocurrido un error");
                         d.resolve(false);
                     }
                 })
                 .error(function(err) {
                     HideLoading();
                     console.log(err);
                     alert(err);
                 });
        } else {
            HideLoading();
            d.resolve(true);
        }

        d.promise.then(function(resultprocess) {
            HideLoading();
            if (resultprocess) {
                $scope.disabledFields(true);
                $scope.toUpdate = false;
            }
        });
    };

    $scope.deleteImg = function (name) {

        var imgToDelete = '';
        if (name === 'img1Name') {
            imgToDelete = $scope.ImageFileName1;
        } else if (name === 'img2Name'){
            imgToDelete = $scope.ImageFileName2;
        } else if (name === 'img3Name'){
            imgToDelete = $scope.ImageFileName3;
        } else if (name === 'img4Name'){
            imgToDelete = $scope.ImageFileName4;
        } else if (name === 'imgSeoName'){
            imgToDelete = $scope.SocialImage;
        } else if (name === 'imgLarge1Name'){
            imgToDelete = $scope.ImageFileNameLarge1;
        } else if (name === 'imgLarge2Name'){
            imgToDelete = $scope.ImageFileNameLarge2;
        } else if (name === 'imgLarge3Name'){
            imgToDelete = $scope.ImageFileNameLarge3;
        } else if (name === 'imgLarge4Name'){
            imgToDelete = $scope.ImageFileNameLarge4;
        }

        ShowLoading();
        $http.get('API/product/deleteimage/' + $scope.variantId + '/' + name + '/' + imgToDelete)
             .success(function(data) {
                 HideLoading();
                 if (name === 'img1Name') {
                     $scope.ImageFileName1 = '';
                     $scope.imgObj.img1 = false;
                     $("#preview-1-img").attr('src', '');
                 } else if (name === 'img2Name'){
                     $scope.ImageFileName2 = '';
                     $scope.imgObj.img2 = false;
                     $("#preview-2-img").attr('src', '');
                 } else if (name === 'img3Name'){
                     $scope.ImageFileName3 = '';
                     $scope.imgObj.img3 = false;
                     $("#preview-3-img").attr('src', '');
                 } else if (name === 'img4Name'){
                     $scope.ImageFileName4 = '';
                     $scope.imgObj.img4 = false;
                     $("#preview-4-img").attr('src', '');
                 } else if (name === 'imgSeoName'){
                     $scope.SocialImage = '';
                     $scope.imgObj.imgSeo = false;
                     $("#preview-seo-img").attr('src', '');
                 } else if (name === 'imgLarge1Name'){
                     $scope.ImageFileNameLarge1 = '';
                     $scope.imgLargeObj.imgLarge1 = false;
                     $("#preview-large-1-img").attr('src', '');
                 } else if (name === 'imgLarge2Name'){
                     $scope.ImageFileNameLarge2 = '';
                     $scope.imgLargeObj.imgLarge2 = false;
                     $("#preview-large-2-img").attr('src', '');
                 } else if (name === 'imgLarge3Name'){
                     $scope.ImageFileNameLarge3 = '';
                     $scope.imgLargeObj.imgLarge3 = false;
                     $("#preview-large-3-img").attr('src', '');
                 } else if (name === 'imgLarge4Name'){
                     $scope.ImageFileNameLarge4 = '';
                     $scope.imgLargeObj.imgLarge4 = false;
                     $("#preview-large-4-img").attr('src', '');
                 }

                 $("#"+name).val('');
                 alert("Imagen actualizada.");
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });

        return false;
    };

    $scope.previewImg = function (name) {
        $("#"+name).attr('style', 'display:inherit');
        if (name === 'preview-1') {
            $("#"+name+"-img").attr('src', $scope.baseURL+$scope.ImageFileName1);
        } else if (name === 'preview-2'){
            $("#"+name+"-img").attr('src', $scope.baseURL+$scope.ImageFileName2);
        } else if (name === 'preview-3'){
            $("#"+name+"-img").attr('src', $scope.baseURL+$scope.ImageFileName3);
        } else if (name === 'preview-4'){
            $("#"+name+"-img").attr('src', $scope.baseURL+$scope.ImageFileName4);
        } else if (name === 'preview-seo'){
            $("#"+name+"-img").attr('src', $scope.baseURL+$scope.SocialImage);
        } else if (name === 'preview-large-1'){
            $("#"+name+"-img").attr('src', $scope.baseURL+$scope.ImageFileNameLarge1);
        } else if (name === 'preview-large-2'){
            $("#"+name+"-img").attr('src', $scope.baseURL+$scope.ImageFileNameLarge2);
        } else if (name === 'preview-large-3'){
            $("#"+name+"-img").attr('src', $scope.baseURL+$scope.ImageFileNameLarge3);
        } else if (name === 'preview-large-4'){
            $("#"+name+"-img").attr('src', $scope.baseURL+$scope.ImageFileNameLarge4);
        }

        return false;

    };

    if ($location.path().indexOf('/product-stock') >= 0 ) {

        var dd = $q.defer();
        ShowLoading();
        $http.get('API/categories/getcategories/')
             .success(function(data) {
                 HideLoading();
                 $scope.categoryList = data;
                 $scope.categoryList.push({"CategoryID" :  0, "Category" : "TODOS" });
                 dd.resolve(true);
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });
        dd.promise.then(function(validList) {
            HideLoading();
            if (validList) {
                $scope.CategoryID = {"CategoryID" :  0};
            }
        });
    }

    $scope.IsOutOfStockEvent = function (item, newstatus, valuenewstatus) {

        ShowLoading();
        $http.get('API/product/variant_outofstockstatus/' + item + '/' + valuenewstatus)
             .success(function(data) {
                 HideLoading();
                 $('#btnDefault' + item).html(newstatus);

                 if ($location.path().indexOf('/product-stock') >= 0 ) {
                     $('#txtVariant' + item).val('0');
                 }

                 alert('Item actualizado');
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });

    };


    $scope.EnabledEvend = function (item, newstatus, valuenewstatus) {
        ShowLoading();
        $http.get('API/product/variant_enablestatus/' + item + '/' + valuenewstatus)
             .success(function(data) {
                 HideLoading();
                 $('#btnEnabled' + item).html(newstatus);
                 alert('Item actualizado');
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });

    };

    $scope.ShowStatusStock = function (isoutofstock) {
        if(isoutofstock==1){
            return 'Si';
        }
        else{
            return 'No';
        }
    };

    $scope.ShowStatusEnabled = function (status) {
        if(status==0){
            return 'No';
        }
        else{
            return 'Si';
        }
    };

    /*Sizes*/

    //Function modalVariantSize
    $scope.modalVariantSize = function(){

        $scope.isnewsize = true;
        $scope.sizes = {};
        //Get all brands
        ShowLoading();
        $http.get('API/size/size_get_available_for_variant/'+ $routeParams.variantID)
             .success(function(data) {
                 HideLoading();
                 $scope.sizes = data;
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });

        $scope.formVariant = $scope.variant;
        $scope.formVariant.variantId = 0;
        $scope.formVariant.Code = '';
        $scope.formVariant.Price = '';
        $scope.lblSizeName = '';


        //Open Layout
        var options = 'toggle';
        $('#ModalSize').modal(options);

    };

    $scope.loadVariantSize = function(variantsizeid){

        $scope.isnewsize = false;
        $scope.sizes = {};
        //Get all brands
        ShowLoading();
        $http.get('API/product/getvariantbyid/' + variantsizeid)
             .success(function(data) {
                 $http.get('API/size/size_get_available_for_variant/'+ $routeParams.variantID)
                      .success(function(dataSize) {
                          HideLoading();
                          $scope.sizes = dataSize;
                          $scope.sizes.push({"SizeID" :  data.SizeID, "SizeName" : data.Size });

                          $scope.formVariant = data;
                          $scope.lblSizeName = data.Size;
                          $scope.formVariant.Name= $scope.formVariant.Name.replace(data.Size,'') ;
                          $scope.formVariant.Size = {"SizeID": $scope.formVariant.SizeID, "SizeName": $scope.formVariant.SizeName};

                      })
                      .error(function(err) {
                          HideLoading();
                          console.log(err);
                          alert(err);
                          return;
                      });

             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });

        //Open Layout
        var options = 'toggle';
        $('#ModalSize').modal(options);

    };

    $scope.SaveSize = function(){
        //Validate BrandID input
        if (typeof($('#ddlSize').val())=="undefined" || $('#ddlSize').val() === "") {
            alert("Debe escoger la talla");
            return;
        }
        if ( $scope.variantForm.$invalid ){
            alert("Existen campos con error. \nRevisar el formulario.");
            return;
        }

        var variant;

        if($scope.isnewsize){
            variant = {
                pid  : $scope.variant.ProductID,
                vid  : 0,
                name : $scope.variant.Name + ' ' + $scope.lblSizeName,
                sku  : $scope.formVariant.Code,
                price: $scope.formVariant.Price,
                parentvariantid: $scope.variant.VariantID,
                sizeid: $('#ddlSize').val()

            };
        }
        else
            {
                variant = {
                    pid  : $scope.variant.ProductID,
                    vid  : $scope.formVariant.VariantID,
                    name : $scope.variant.Name + ' ' + $scope.lblSizeName,
                    sku  : $scope.formVariant.Code,
                    price: $scope.formVariant.Price,
                    parentvariantid: $scope.variant.VariantID,
                    sizeid: $('#ddlSize').val()
                };
            }

        ShowLoading();
        $http.post('API/product/insertvariantsize/', variant)
             .success(function(data) {
                 if (parseInt(data) > 0) {
                     //Close Modal
                     var options = 'hide';
                     $('#ModalSize').modal(options);
                     $('body').removeClass('modal-open');
                     $('.modal-backdrop').remove();
                     alert("Item guardado");

                     $http.get('API/product/product_getvariantsizes_admin/' + $routeParams.productID + '/' + $routeParams.variantID)
                          .success(function(data) {
                              HideLoading();
                              $scope.variants = data;
                          })
                          .error(function(err) {
                              HideLoading();
                              console.log(err);
                              alert(err);
                          });

                 } else {
                     HideLoading();
                     alert("Ocurrio un error inesperado!");
                 }
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
                 return;
             });
    };

    $scope.getSize = function(item){
        for (index = 0; index < $scope.sizes.length; ++index) {
            if($scope.sizes[index]["SizeID"]==item){
                $scope.lblSizeName = $scope.sizes[index]["SizeName"];
                return;
            }
        }
    };

    /*Sizes End*/


    $scope.loadProductShippingInfo = function(product){
        $scope.ProductShippingInfo = {};
        $scope.ProductBasicShippingInfo = {};

        $('#hdnProductId').val(product.ProductID);
        $scope.ProductName = product.Name;
        $scope.ProductBasicShippingInfo = product;

        ShowLoading();
        $http.get('API/productshippinginfo/product_getshippinginfo/' + product.ProductID + '/')
             .success(function(data) {
                 HideLoading();
                 $scope.ProductShippingInfo = data[0];
                 var options = 'toggle';
                 $('#myModalProductShippingInfo').modal(options);

             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
                 return;
             });
    };

    $scope.updateProductShippingInfo = function() {

        if(($('#txtHeight').val() === parseFloat($('#txtHeight').val(),10)) || (parseFloat($('#txtHeight').val(),10) <= 0) ) {
            alert('Debe ingresar un alto valido');
            return;
        }

        if(($('#txtLength').val() === parseFloat($('#txtLength').val(),10)) || (parseFloat($('#txtLength').val(),10) <= 0) ) {
            alert('Debe ingresar un largo valido');
            return;
        }

        if(($('#txtWidth').val() === parseFloat($('#txtWidth').val(),10)) || (parseFloat($('#txtWidth').val(),10) <= 0) ) {
            alert('Debe ingresar un ancho valido');
            return;
        }

        if(($('#txtWeight').val() === parseFloat($('#txtWeight').val(),10)) || (parseFloat($('#txtWeight').val(),10) <= 0) ) {
            alert('Debe ingresar un peso valido');
            return;
        }


        var args = {
            ProductID : $('#hdnProductId').val(),
            Height : $('#txtHeight').val(),
            Length : $("#txtLength").val(),
            Weight : $("#txtWeight").val(),
            Width : $("#txtWidth").val()
        };
        ShowLoading();
        $http.post('API/productshippinginfo/product_insertshippinginfo/', args)
             .success(function(data) {
                 HideLoading();
                 var options = 'hide';
                 $('#myModalProductShippingInfo').modal(options);
                 $('body').removeClass('modal-open');
                 $('.modal-backdrop').remove();
                 $('#lblAlert'+ $('#hdnProductId').val()).hide();
                 $('#hdnProductId').val(0);
                 $scope.ProductName = '';
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
                 return;
             });
    };

    $scope.loadProductExtraInfo = function(product){
        $scope.ProductExtraInfo = {};

        $('#hdnProductId').val(product.ProductID);
        $scope.ProductName = product.Name;
        $scope.ProductURL = product.URL;

        var i = 0;
        var contentselected;
        for(i; i < $scope.contents.length; i++ ){
            if($scope.contents[i].ContentID == product.ContentID){
                contentselected = $scope.contents[i];
                break;
            }
        }

        $scope.content.content = {"ContentID": contentselected.ContentID, "ContentName": contentselected.ContentName};

        $scope.ProductExtraInfo = product;

        var options = 'toggle';
        $('#myModalProductExtraInfo').modal(options);

    };


    $scope.product_update_content = function () {
        ShowLoading();
        $http.get('API/product/product_update_content/' + $('#hdnProductId').val() + '/' + $('#ddlContent').val())
             .success(function(data) {
                 HideLoading();
                 alert('Contenido Actualizado');
                 $scope.searchProductByName();
                 var options = 'hide';
                 $('#myModalProductExtraInfo').modal(options);
                 $('body').removeClass('modal-open');
                 $('.modal-backdrop').remove();
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
                 return;
             });

    };

    $scope.EditZoomImages = function (Variant) {
        $scope.baseURL = Variant.BASEURL;
        $scope.imgLargeObj = {};
        $scope.imgLargeObj.imgLarge1 = false;
        $scope.imgLargeObj.imgLarge2 = false;
        $scope.imgLargeObj.imgLarge3 = false;
        $scope.imgLargeObj.imgLarge4 = false;

        $scope.variantId = Variant.VariantID;

        $("#preview-large-1").attr('style', 'display:none');
        $("#preview-large-2").attr('style', 'display:none');
        $("#preview-large-3").attr('style', 'display:none');
        $("#preview-large-4").attr('style', 'display:none');
        $("#preview-large-1-img").attr('src', '');
        $("#preview-large-2-img").attr('src', '');
        $("#preview-large-3-img").attr('src', '');
        $("#preview-large-4-img").attr('src', '');
        $("#imgLarge1Name").val('');
        $("#imgLarge2Name").val('');
        $("#imgLarge3Name").val('');
        $("#imgLarge4Name").val('');

        $scope.lblVariantName = Variant.Name;

        var options = 'toggle';
        $('#myModalZoom').modal(options);



        /*
         * $scope.imgLargeObj.imgLarge4*/
        if (Variant.ImageFileNameLarge1 !== null && Variant.ImageFileNameLarge1 !== '') {
            $scope.ImageFileNameLarge1 = Variant.ImageFileNameLarge1;
            $("#imgLarge1Name").val((Variant.ImageFileNameLarge1).replace("images_product/", ""));
            $scope.imgLargeObj.imgLarge1 = true;
        }

        if (Variant.ImageFileNameLarge2 !== null && Variant.ImageFileNameLarge2 !== '') {
            $scope.ImageFileNameLarge2 = Variant.ImageFileNameLarge2;
            $("#imgLarge2Name").val((Variant.ImageFileNameLarge2).replace("images_product/", ""));
            $scope.imgLargeObj.imgLarge2 = true;
        }

        if (Variant.ImageFileNameLarge3 !== null && Variant.ImageFileNameLarge3 !== '') {
            $scope.ImageFileNameLarge3 = Variant.ImageFileNameLarge3;
            $("#imgLarge3Name").val((Variant.ImageFileNameLarge3).replace("images_product/", ""));
            $scope.imgLargeObj.imgLarge3 = true;
        }

        if (Variant.ImageFileNameLarge4 !== null && Variant.ImageFileNameLarge4 !== '') {
            $scope.ImageFileNameLarge4 = Variant.ImageFileNameLarge4;
            $("#imgLarge4Name").val((Variant.ImageFileNameLarge4).replace("images_product/", ""));
            $scope.imgLargeObj.imgLarge4 = true;
        }

    };

    $scope.UpdateZoomImages = function () {

        //Save Img
        var imgLarges = {
            id : 0,
            name : $scope.lblVariantName,
            imgLarge1Name : $("#imgLarge1Name").val(),
            imgLarge2Name : $("#imgLarge2Name").val(),
            imgLarge3Name : $("#imgLarge3Name").val(),
            imgLarge4Name : $("#imgLarge4Name").val(),
            imgLarge1 : $scope.zoomForm.imgLarge1,
            imgLarge2 : $scope.zoomForm.imgLarge2,
            imgLarge3 : $scope.zoomForm.imgLarge3,
            imgLarge4 : $scope.zoomForm.imgLarge4
        };
        ShowLoading();
        $http.post('API/uploader/imgzoomuploader/', imgLarges)
             .success(function(data) {

                 //Save Variant
                 var variant = {

                     vid  : $scope.variantId,
                     name : $scope.lblVariantName
                 };

                 $http.post('API/product/variant_imagezoom_update/', variant)
                      .success(function(data) {
                          HideLoading();
                          if (parseInt(data) > 0) {
                              //Close Modal
                              var options = 'hide';
                              $('#myModalZoom').modal(options);
                              $('body').removeClass('modal-open');
                              $('.modal-backdrop').remove();
                              alert("Item guardado");
                              if ($location.path() == '/product-edit/'+$scope.prodId+'') {
                                  //Refresh Variants List
                                  $scope.getVariantsByProductId();
                                  //Clean Form
                                  $("#imgLarge1Name").val('');
                                  $("#imgLarge2Name").val('');
                                  $("#imgLarge3Name").val('');
                                  $("#imgLarge4Name").val('');
                                  $scope.zoomForm = {};
                              } else {
                                  $location.path('/product-edit/'+$scope.prodId).replace();
                              }
                          } else {
                              alert("Ocurrio un error inesperado!");
                          }
                      })
                      .error(function(err) {
                          HideLoading();
                          console.log(err);
                          alert(err);
                          return;
                      });
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
                 return;
             });

    };

    $scope.EnableTextBox = function(id){
        if( $('#chkVariant' + id).is(":checked"))
            $('#txtVariant'+ id).prop("disabled", false);
        else
            $('#txtVariant'+ id).attr('disabled', 'disabled');
    };

    $scope.EnableAllTextBox = function(){
        if( $('#chkVariant').is(":checked")){
            $(".chkVariant").prop('checked', true);
            $('.txtVariant').prop("disabled", false);
        }
        else{
            $(".chkVariant").prop('checked', false);
            $('.txtVariant').attr('disabled', 'disabled');
        }
    };


    //Function to Search Products
    $scope.products_manager_getforstock = function(){

        $('#divEmptyResults').hide();
        $('.btnSave').hide();
        ShowLoading();
        $http.get('API/product/products_manager_getforstock/' + $('#ddlStock').val() + '/' + $('#ddlCategory').val() + '/' + $("#productName").val())
             .success(function(data) {
                 HideLoading();
                 if(data == null || data.lenght==0){
                     $('#divEmptyResults').show();
                 }
                 else
                     $('.btnSave').show();

                 $scope.productList = data;

             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });
    };

    $scope.saveStock = function (){

        var selected = $("input:checked").length;
        if(selected==0)
            {
                alert('Debe activar al menos un producto.');
                return;
            }

        var validData = true;
        $("input:checked").each(function () {
            var id = $(this).attr("ref");
            if(this.checked && id != 0){
                if(($('#txtVariant'+ id).val() === parseFloat($('#txtVariant'+ id).val(),10)) || (parseFloat($('#txtVariant'+ id).val(),10) < 0)) {
                    alert('Debe ingresar un stock valido:  mayor a 0');
                    $('#txtVariant'+ id).focus();
                    validData = false;
                    return;
                }
            }
        });

        if(!validData)
            return;


        if(confirm("Seguro que desear grabar los stock?"))
            {
                $scope.OBJ = {};
                $scope.OBJ.products = [];

                $("input:checked").each(function () {
                    var id = $(this).attr("ref");
                    if(this.checked && id != 0)
                        $scope.OBJ.products.push({"variantid" : id, "stock": $('#txtVariant'+ id).val() });
                });

                ShowLoading();
                $http.post('API/product/variant_set_stock', $scope.OBJ)
                     .success(function(data) {
                         HideLoading();
                         $scope.products_manager_getforstock();
                         alert('Stock(s) grabados.');
                     })
                     .error(function(err) {
                         HideLoading();
                         console.log(err);
                         alert(err);
                         return;
                     });
            }
    };

    $scope.UpdateVariantStatus = function(variantid,productid) {

        var Objdata = {
            VariantID: variantid,
            ProductID: productid,
            Enabled: $("#chkVariantStatusEnable" + variantid).is(":checked")
        };

        ShowLoading();
        $('#divEmptyResults').hide();
        $http.post('API/product/product_update_enabled/', Objdata)
             .success(function(res) {
                 HideLoading();
                 alert('Estado Cambiado.');
                 window.location.href = '#/product-edit/' + $routeParams.productID + '';
                 $('#divShowResultsTable').show();
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
                 $('#divShowResultsTable').show();
             });
    };

    $scope.UpdateVariantStatus = function (newstatus, valuenewstatus, variantid, productid) {

        var Objdata = {
            VariantID: variantid,
            ProductID: productid,
            Enabled: valuenewstatus
        };

        ShowLoading();
        $http.post('API/product/product_update_enabled/', Objdata)
             .success(function(data) {
                 HideLoading();
                 $('#btnDefaultStatus' + variantid).html(newstatus);
                 alert('Estado Cambiado.');
                 $scope.getVariantsByProductId();
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });

    };

    $scope.ShowEnabled = function (enabled) {
        if(enabled==1){
            return 'Si';
        }
        else{
            return 'No';
        }
    };

    $scope.getVariantColor = function (variant) {

        if(variant.Color === '' || variant.Color === null || typeof(variant.Color) === 'undefined'){
           $("#selectectColor").spectrum({
                color: "#fff"
            });
        } else {

            $("#selectectColor").spectrum({
                color: variant.Color
            });
        }
        // para salvar: $("#selectectColor").spectrum('get').toHexString();

    };

    $scope.saveVariantColor = function (variant) {
        /*  0 = seleccionar color
         *  1 = colores por defecto */
        var Objdata = {};

        if($scope.ddlcolormode === '1') {

            Objdata = {
                ProductID: variant.ProductID,
                VariantID: variant.VariantID,
                Color: variant.Color
            };

        } else {

            var color = $("#selectectColor").spectrum('get').toHexString();

            Objdata = {
                ProductID: variant.ProductID,
                VariantID: variant.VariantID,
                Color: color
            };

        }

        var c = confirm("Seguro que desea cambiar el color");

        if(c === true) {
            ShowLoading();
            $http.post('API/variants/variants_update_color/', Objdata)
                 .success(function(res) {
                     HideLoading();
                     $('#myModalColor').modal('toggle');
                     $scope.getVariantsByProductId();
                 })
                 .error(function(err) {
                     HideLoading();
                     console.log(err);
                     alert(err);
                 });
        }
    };

    $scope.showColorPopUp = function (variantObj) {

        $('#myModalColor').modal('toggle');
        $scope.variantpu = variantObj;
        $scope.ddlcolormode = '1';
        var results = [];
        var searchField = "ColorValue";
        var searchVal = variantObj.Color;
        for (var i=0 ; i < $scope.colorList.length ; i++)
        {
            if ($scope.colorList[i][searchField] === searchVal) {
               results.push($scope.colorList[i]);
               $scope.variantpu.ColorName = results[0].ColorName;
            }
            if($scope.variantpu.ColorName === '' ||
               typeof $scope.variantpu.ColorName === 'undefined'
            ) {
                $scope.ddlcolormode = '0';
            } else {
                $scope.ddlcolormode = '1';
            }

        }

        if (typeof $scope.variantpu.ColorValue === 'undefined' &&
            $scope.variantpu.Color === null
        ) {
            $scope.ddlcolormode = '1';
        }

        $scope.getVariantColor(variantObj);
    };

    $scope.scolor = '';

    $scope.findsearch = function(){
        var search = {
            ColorName: $scope.scolor
        };
        ShowLoading();
        $http.post('API/colors/color_getall/',search)
             .success(function(data) {
                HideLoading();
                $scope.colorList = data;
             })
             .error(function(err) {
                 alert(err);
             });
    };

    $scope.disableProduct = function(product){
        if(confirm("¿Seguro que deshabilitar todo el producto?")) {
            ShowLoading();
            $http.get('API/product/disable_all/' + product.ProductID + '/')
            .success(function(data) {
                HideLoading();
                $scope.getVariantsByProductId();
            })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });
        }
    };


    $scope.categoriesGetForProducts = function(product){
        $scope.productSelected = product;
        $scope.ProductName = product.Name;
        $scope.ProductURL = product.URL;

        var options = 'toggle';
        $('#myModalCategoriesByProduct').modal(options);

        $scope.loadCategoriesForProduct(product);
    };

    $scope.loadCategoriesForProduct = function(product){


        ShowLoading();
        $http.get('API/categories/categories_get_by_productid/' + product.ProductID + '/')
             .success(function(data) {
                 HideLoading();



                 $scope.categoriesForProductList = data;

                 if(parseInt(data.length,10)>0){
                     $scope.lblMessageforCategory = '';
                 }
                 else{
                     $scope.lblMessageforCategory = 'No existen registros para mostrar';
                 }
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });
    };


    $scope.deleteProductFromCategory = function(categoryID){

        if(confirm("Seguro que desea eliminar la categoría?")){

        ShowLoading();
        $http.get('API/categories/category_removeproduct/' + categoryID + '/' + $scope.productSelected.ProductID + '/')
             .success(function(data) {
                 HideLoading();

                 $scope.loadCategoriesForProduct($scope.productSelected);
             })
             .error(function(err) {
                 HideLoading();
                 console.log(err);
                 alert(err);
             });
         }
    };

    $scope.findsearch();

    //to addVariant
    if ($location.path() == '/product-edit/'+$routeParams.productID+'') {
        $scope.IsDimensionEnabled = false;
    }
    else {
        $scope.IsDimensionEnabled = true;
    }

    $scope.showHeelSizePopUp = function(data) {

        $('#myModalPopUpHeelSize').modal();

        if($location.path() == '/product-edit/'+$routeParams.productID+'') {
            $scope.ProductIDOnHeelSizePopUp = $routeParams.productID;
        }
        else {
            $scope.ProductIDOnHeelSizePopUp = data.ProductID
        }

        var getter = {
            ProductID: $scope.ProductIDOnHeelSizePopUp
        };

        ShowLoading();
        $http.post('API/products/products_get_heelsizeid/', getter)
            .success(function(data) {
                HideLoading();
                if(data != null || typeof data != 'undefined') {
                    $scope.HeelSizeOfVariantsID = data;
                    $('#txtHeelSizeID').val($scope.HeelSizeOfVariantsID);
                }
            })
            .error(function(err) {
                HideLoading();
                console.log(err);
                alert(err);
                return;
            });
    };

    if ($location.path() == '/product-admin' ||
        $location.path() == '/product-edit/'+$routeParams.productID+'') {
        ShowLoading();
        $http.post('API/heelsizes/heelsize_get_all/')
            .success(function(data) {
                HideLoading();
                $scope.heelsizeList = data;
                if(data == null ||
                   typeof data == 'undefined')
                    $scope.lblNoHeelSizeExists = 'No ha agregado tallas de taco en el administrador';
            })
            .error(function(err) {
                HideLoading();
                console.log(err);
                alert(err);
                return;
            });
    }

    $scope.updateProductHeelSize = function() {

        if($scope.HeelSizeID == null ||
           typeof $scope.HeelSizeID == 'undefined') {
            alert('Eliga una talla de taco');
            return;
        }

        var updater = {
            HeelSizeID: $scope.HeelSizeID,
            ProductID: $scope.ProductIDOnHeelSizePopUp
        };

        ShowLoading();
        $http.post('API/products/products_update_heelsizeid/', updater)
            .success(function(data) {
                HideLoading();
                $('#myModalPopUpHeelSize').modal('toggle');
                alert('Tamaño de taco guardado');
            })
            .error(function(err) {
                HideLoading();
                console.log(err);
                alert(err);
                return;
            });

    };

}]);
