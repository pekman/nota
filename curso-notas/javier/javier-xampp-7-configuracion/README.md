# XAMPP CONFIGURACION PARA 7.2


## non-free xD!!!


### Parte 1


- Remplazar esto en el `/opt/lampp/etc/httpd.conf` :


        <Directory />
            Allowoverride ALL
            Allow from all
            Require all granted
        </Directory>


- Descomentar esto en el `/opt/lampp/etc/httpd.conf` tambien :


        Include etc/extra/httpd-vhosts.conf


### Parte 2


- Como ya se activaron los virtual hosts en la parte 1 rooteamos los folders de los proyectos en `/opt/lampp/etc/extra/httpd-vhosts.conf` :


        <VirtualHost *:80>
            ServerAdmin webmaster@site.s3videomanager.test
            DocumentRoot "/home/sl/KF/11Y6/FULL-7/site/code"
            ServerName site.s3videomanager.test
            ServerAlias www.site.s3videomanager.test
            ErrorLog "logs/site.s3videomanager.test-error_log"
            CustomLog "logs/site.s3videomanager.test-access_log" common
        </VirtualHost>


### Parte 3 php


- El xampp 7.2 muestra los notices, warnings, etc. entonces modificamos esta linea en el config de php del lampp `/opt/lampp/etc/php.ini` :


- en la seccion:


        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ; Error handling and logging ;
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


- la linea que dice esto: `error_reporting=E_ALL & ~E_DEPRECATED & ~E_STRICT`


- remplazarla por la siguiente:


        error_reporting=E_COMPILE_ERROR|E_RECOVERABLE_ERROR|E_ERROR|E_CORE_ERROR


- ^ eso hará que sólo popule los errores el php7.2



### Parte 4


- para revisar el registro de errores


        tail -f /opt/lampp/logs/site.s3videomanager.test-error_log | ccze -A


- ^ requiere instalar ccze ejm. `sudo apt install ccze`



- para guardar un backup se recomienda, o revisar los al comenzar el debuggeo


        sudo mv /opt/lampp/logs/site.s3videomanager.test-error_log /opt/lampp/logs/site.s3videomanager.test-error_log.$(date "+%F_%T").old
