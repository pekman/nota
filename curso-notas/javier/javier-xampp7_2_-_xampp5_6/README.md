# INSTALAR XAMPP5.6 Y XAMPP7.2 EN LA MISMA MAQUINA


### TECNICAS GLAMPP


## Purgar apache* y php*


#### Tener en cuenta que primero podríamos seguir la guía de mysql


## apache*


- purgar apache*


`sudo apt purge apache*`


`sudo apt autoremove`


## php*


- purgar php*


`sudo apt purge php*`


`sudo apt autoremove`


## El xampp que tenemos actuamente debe renombrarse para poder alternarlo posteriormente


`sudo mv /opt/lampp /opt/lampp5_6`


## Una vez renombrado procedemos con la instalación del php7


- descargar el binario `https://www.apachefriends.org/es/download.html#download-linux`


- ir a la carpeta downloads dale permisos


- `sudo chmod +x ./xampp-linux-x64-7.2.31-0-installer.run`


- ejecutarlo como sudo


- `sudo ./xampp-linux-x64-7.2.31-0-installer.run`


## Decirle a ubuntu que ejecute php de xampp y no el nativo (por que ya no lo tiene tambien)


- `sudo ln -s /opt/lampp/bin/php /usr/bin/php`


- nota ^ : "no va aver problema cuando alternemos de xampps, por que la ubicación siempre será la misma"


## Ahora un comando que nos ayudará a ejecutar xampp 5.6 o xampp 7.2 a nuestra necesidad desde terminal


- agregamos esto en un archivo (nuevo por si no lo tenemos) de este nombre `~/.bashrc` lo siguiente:

```
kflampp5(){
    if [[ -d "/opt/lampp7_2" ]]; then
        printf "Estas en la version 5.6 de XAMPP, Quizá deberías hacer kfstart\n"
    else
        sudo systemctl stop mysql &&
        sudo /opt/lampp/bin/httpd -k stop &&
        sudo mv -v /opt/lampp /opt/lampp7_2 &&
        sudo mv -v /opt/lampp5_6 /opt/lampp
    fi;
}

kflampp7(){
    if [[ -d "/opt/lampp5_6" ]]; then
        printf "Estas en la version 7.2 de XAMPP, Quizá deberías hacer kfstart\n";
    else
        sudo systemctl stop mysql &&
        sudo /opt/lampp/bin/httpd -k stop &&
        sudo mv -v /opt/lampp /opt/lampp5_6 &&
        sudo mv -v /opt/lampp7_2 /opt/lampp
    fi;
}

alias kfstart='sudo /opt/lampp/bin/httpd -k start && sudo systemctl start mysql'

alias kfrestart='sudo /opt/lampp/bin/httpd -k restart && sudo systemctl restart mysql'

alias kfstop='sudo /opt/lampp/bin/httpd -k stop && sudo systemctl stop mysql'

```

- cerrar el emulador de terminal y volver a abrir

## los comandos ahora disponibles son los siguientes

### nota "deberíamos haber instalado mysql 5.6 antes de ejecutar el interruptor de xampps"


- los siguientes son los comandos que tenemos disponibles (se puede apretar TAB para autocompletarlos todos comienzan por kf)


- comandos:


- `kflampp5`  -->  este comando apaga xampp 7 y prende el 5 + servicio mysql


- `kflampp7`  --> este comando apaga el xampp 5 y prende el 7 + servicio mysql


- `kfstart`   --> enciende el xampp que tengamos disponible + el servicio mysql


- `kfrestart` --> reinicia el xampp que tengamos disponible + el servicio mysql


- `kfstop`    --> detiene el xampp que tengamos disponible + el servicio mysql


## deberiamos apagar el servicio del mysql y solo encenderlo con los comandos kf


- apagamos en encendedor automatico de mysql al encender


- `sudo systemctl disable mysql`


- apagamos mysql si está corriendo


- `sudo systemctl stop mysql`


## ahora podemos usar el comando kf


- `kflampp5`


- ^ asta aqui deberiamos haber exportado nuestras bases de datos al nuevo mysql para que funcionen los proyectos del xampp 5.6 que ya teniamos funcionando


- la guía para [configurar xampp 7.2](https://notabug.org/saravia/nota/src/master/javier-xampp-7-configuracion)
