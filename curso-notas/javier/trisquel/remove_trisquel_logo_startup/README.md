# Remove Ubuntu Start Up Logo


Trisquel logo startup delay the start up need remove to optimize ubuntu gnu/linux O.S.


```
sudo apt purge plymouth
```


Yes. Edit /etc/default/grub (using gksu gedit /etc/default/grub), and remove the "quiet splash" from the Linux command line:
Here's what it looks like by default:


```
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"
```


Make it look like this:


```
GRUB_CMDLINE_LINUX_DEFAULT=""
```


After this run:


```
sudo update-grub2
```


Reboot login and run:


```
sudo apt autoremove
```
