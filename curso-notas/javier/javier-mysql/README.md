# Install Mysql 5.6 on Ubuntu 18.04 LTS

# non-free xD!!

## Desinstalar mysql

### Open the terminal and follow along:

- Uninstall any existing version of MySQL

```
sudo rm /var/lib/mysql/ -R
```

- Delete the MySQL profile

```
sudo rm /etc/mysql/ -R
```
- Automatically uninstall mysql

```
sudo apt-get autoremove mysql* --purge
sudo apt-get remove apparmor
```

## Descargar paqueteria de debian 9 (sid/bundle)

- "Descargar el tar donde está todo"

```
https://dev.mysql.com/downloads/mysql/5.6.html#downloads
```


OPTIONS:


- 5.6.xx


- Debian Linux


- Debian Linux 9 (x86, 64-bit)

- Descomprimir el tar he ir a la carpeta donde están los .deb en la terminal


## Instalar en orden

```
sudo dpkg -i mysql-community-source_5.6.50-1debian9_amd64.deb mysql-common_5.6.50-1debian9_amd64.deb libmysqlclient18_5.6.50-1debian9_amd64.deb libmysqlclient-dev_5.6.50-1debian9_amd64.deb mysql-common_5.6.50-1debian9_amd64.deb libmysqld-dev_5.6.50-1debian9_amd64.deb mysql-community-client_5.6.50-1debian9_amd64.deb mysql-client_5.6.50-1debian9_amd64.deb mysql-community-server_5.6.50-1debian9_amd64.deb mysql-server_5.6.50-1debian9_amd64.deb mysql-community-bench_5.6.50-1debian9_amd64.deb mysql-community-test_5.6.50-1debian9_amd64.deb mysql-testsuite_5.6.50-1debian9_amd64.deb
```

## Si no tambien pueden ser estos comandos copypaste en ~/install-mysql.sh


    #!/bin/bash

    sudo dpkg -r mysql-community-source && \
    sudo dpkg -r mysql-common && \
    sudo dpkg -r libmysqlclient18 && \
    sudo dpkg -r libmysqlclient-dev && \
    sudo dpkg -r mysql-common.deb && \
    sudo dpkg -r libmysqld-dev && \
    sudo dpkg -r mysql-community-client && \
    sudo dpkg -r mysql-client && \
    sudo dpkg -r mysql-community-server && \
    sudo dpkg -r mysql-server && \
    sudo dpkg -r mysql-community-bench && \
    sudo dpkg -r mysql-community-test && \
    sudo dpkg -r mysql-testsuite.deb && \

    echo '0'
    echo 'install ****************************** mysql-community-source' && \
    sudo dpkg -i mysql-community-source_5.6.50-1debian9_amd64.deb && \
    echo '1'
    echo 'install ****************************** mysql-common' && \
    sudo dpkg -i mysql-common_5.6.50-1debian9_amd64.deb && \
    echo '2'
    echo 'install ****************************** libmysqlclient18' && \
    sudo dpkg -i libmysqlclient18_5.6.50-1debian9_amd64.deb && \
    echo '3'
    echo 'install ****************************** libmysqlclient-dev' && \
    sudo dpkg -i libmysqlclient-dev_5.6.50-1debian9_amd64.deb && \
    echo '4'
    echo 'install ****************************** mysql-common' && \
    sudo dpkg -i mysql-common_5.6.50-1debian9_amd64.deb && \
    echo '5'
    echo 'install ****************************** libmysqld-dev' && \
    sudo dpkg -i libmysqld-dev_5.6.50-1debian9_amd64.deb && \
    echo 'aditional *********root*password****** libncurses5' && \
    sudo apt install libncurses5 && \
    echo '6'
    echo 'install ****************************** mysql-community-client' && \
    sudo dpkg -i mysql-community-client_5.6.50-1debian9_amd64.deb && \
    echo '7'
    echo 'install ****************************** mysql-client' && \
    sudo dpkg -i mysql-client_5.6.50-1debian9_amd64.deb && \
    echo '8'
    echo 'install ****************************** mysql-community-server' && \
    sudo dpkg -i mysql-community-server_5.6.50-1debian9_amd64.deb && \
    echo '9'
    echo 'install ****************************** mysql-server' && \
    sudo dpkg -i mysql-server_5.6.50-1debian9_amd64.deb && \
    echo '10'
    echo 'install ****************************** mysql-community-bench' && \
    sudo dpkg -i mysql-community-bench_5.6.50-1debian9_amd64.deb && \
    echo '11'
    echo 'install ****************************** mysql-community-test' && \
    sudo dpkg -i mysql-community-test_5.6.50-1debian9_amd64.deb && \
    echo '12'
    echo 'install ****************************** mysql-testsuite' && \
    sudo dpkg -i mysql-testsuite_5.6.50-1debian9_amd64.deb
