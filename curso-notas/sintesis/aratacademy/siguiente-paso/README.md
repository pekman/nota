# ¿Por qué el equilibrio es importante en el desarrollo personal?

- Por que cuando las personas están muy enfocadas en el deseo de más,
  entran en una especie de ansiedad por lograr sus objetivos, es ahí
  cuando brota el sentimiento de infelicidad, por que tienen muchos
  proyectos por empezar y normalmente nunca acaban un proyecto; esto
  se conoce también como apatía, una persona apática por ejemplo es
  alguien apegado a no ser recompensado, por ejemplo, nunca quieren
  comprarse nada, es como una apego a no apegarse a las cosas. Por eso
  es recomendable aprender a disfrutar cada recompensa que nos da la
  vida y no rechazar todo como un apático, que puede volverse en
  avaricia; que es el deseo de tener cada vez mas, sin pensar en los
  demás y escondiendo nuestros tesoros del mundo.

# Técnicas para no caer en avaricia o apatía

- Siempre sólo pensar en el paso siguiente práctico, en vez de sólo
  cegarse a encontrar el resultado.

- Ejemplo: Alguien que sueña con escribir una gran obra artística,
  pero vive infeliz por que no tiene la destreza que desea, no tiene
  los instrumentos musicales que desea, entonces siempre está
  deseando hacer de todo para auto financiarse; y esto le conlleva a
  no tener tiempo para perfeccionar su técnica. La consecuencia es que
  el artista nunca está conforme con su forma de financiación entonces
  no escribe sus obras desde un nivel de conciencia mas alto.

- Tal vez nuestro artista se enfoca tanto en su gran obra, entonces se
  estresa en su mismo deseo, si sólo se enfocase en el siguiente paso
  práctico para lograr su objetivo, y agradeciera cada paso que logra,
  quizá andaría más feliz por ahí y estaría mas agradecido con la vida.
