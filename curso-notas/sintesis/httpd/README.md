# Como hacer vhosts en local

- En la clase de hoy, adelantandonos un poco, descubrí que es posible
  acceder desde otra pc hacia una dirección custom desde mi servidor
  hyperbola, todo localmente, el tema es que hay que configurar los
  /etc/hosts en la laptop receptor, o tipiar la ip de mi hyperpc,
  asignara a algun proyecto por ejemplo:

        127.0.0.1     libretube.test
        192.168.1.50  mediagoblin.test

- ^ por ejemplo si tipeo la ip local 192.168.1.50 en otra laptop
  ingresaré a mi mediagoblin.test, pero nó a mi libretube.test, para
  poder acceder al libretube, sólo haría falta intercambiar las ips


## Comienza el hacking para compartir proyectos web en mi servidor local hyperbola

- Paso numero 1:

        ‎[11:06:46 AM] ‎￼jesús‎: El truco está en manejar dos archivos
        ‎[11:06:59 AM] ‎￼jesús‎: /etc/httpd/conf/httpd.conf y /etc/httpd/conf/extra/httpd-vhosts.conf

- Paso numero 2:

        [11:12:48 AM] ‎￼jesús‎: Exacto, editas /etc/httpd/conf/httpd.conf y activas el enlace a  /etc/httpd/conf/extra/httpd-vhosts.conf

- Paso numero 3:

        [11:17:19 AM] ‎￼jesús‎: luego si deseas entrar a cada sitio con un alias, necesitas editar el archivo /etc/hosts
        ‎[11:17:33 AM] ‎￼jesús‎: Ejemplo:

        # virtualhost
        127.0.0.1       libretube.local
        127.0.0.1       lablibre.local
        127.0.0.1       cl.local
        # End or file

- Paso numero 4:

        [11:20:12 AM] ‎￼supersaravia‎: listo ahora iría a httpd-vhost.conf ✓
        ‎[11:20:26 AM] ‎￼jesús‎: yes
        ‎[11:20:40 AM] ‎￼jesús‎: ejemplo de httpd-vhost.conf
        ‎[11:21:40 AM] ‎￼jesús‎: <VirtualHost *:80>
            DocumentRoot "/home/hyperpath/yoursite"
            ServerName site.local
            ServerAdmin saravia@riseup.net
            ErrorLog "/var/log/httpd/yoursite-error_log"
            CustomLog "/var/log/httpd/yoursite-access_log" common
            <Directory "/home/hyperpath/yoursite">
                Options Indexes FollowSymLinks MultiViews
                AllowOverride all
                Order allow,deny
                Allow from all
                Require all granted
            </Directory>
        </VirtualHost>

- Paso numero 5:

        [11:27:30 AM] ‎￼supersaravia‎: mmm me dice acceso prohivido ✓
        ‎[11:28:24 AM] ‎￼* jesús is back
        ‎[11:28:26 AM] ‎￼jesús‎: forbidden
        ‎[11:28:31 AM] ‎￼jesús‎: ×D
        ‎[11:29:29 AM] ‎￼jesús‎: El mensaje de forbidden aparece cuando apache no es capaz de acceder a la ruta del path porque ese path no está en el grupo http
        ‎[11:30:27 AM] ‎￼jesús‎: Este problema es común cuando el path es de la forma '/home/path/site'
        ‎[11:30:44 AM] ‎￼jesús‎: Ya que apache solo puede acceder de forma predeterminada a '/srv/http/'

- Paso numero 6:

        [11:32:06 AM] ‎￼jesús‎: Es algo 'complejo'
        ‎[11:32:27 AM] ‎￼supersaravia‎: ya veo pero algregandolo al grupo la carpeta no he logrado acceder ✓
        ‎[11:32:52 AM] ‎￼jesús‎: Exacto
        ‎[11:33:02 AM] ‎￼supersaravia‎: : o  : o  : o ✓
        ‎[11:33:05 AM] ‎￼supersaravia‎: mmmm interesante ✓
        ‎[11:33:11 AM] ‎￼supersaravia‎: falta algo aún ✓
        ‎[11:33:14 AM] ‎￼jesús‎: eso es por la ruta primaria debe estar en el grupo http
        ‎[11:33:43 AM] ‎￼supersaravia‎: la ruta primaria cual sería doc? ✓
        ‎[11:34:04 AM] ‎￼supersaravia‎: ~/Público? ￼ ✓
        ‎[11:34:04 AM] ‎￼jesús‎: /home/user
        ‎[11:34:08 AM] ‎￼supersaravia‎: ha user ✓
        ‎[11:34:16 AM] ‎￼jesús‎: ejemplo:
        $ stat /home/libretube/
          Fichero: /home/libretube/
          Tamaño: 4096      	Bloques: 8          Bloque E/S: 4096   directorio
        Dispositivo: 803h/2051d	Nodo-i: 34603009    Enlaces: 6
        Acceso: (0755/drwxr-xr-x)  Uid: ( 1001/libretube)   Gid: (   33/    http)
              Acceso: 2020-06-14 21:41:11.949800440 -0500
        Modificación: 2020-06-13 14:24:33.989830147 -0500
              Cambio: 2020-06-13 14:24:33.989830147 -0500
            Creación: -


- Paso numero 7:

        [11:37:06 AM] ‎￼jesús‎: Está bien pero recomiendo no usar tildes para evitar problemas de carácteres extraños
        ‎[11:37:17 AM] ‎￼jesús‎: podría ser
        ‎[11:37:30 AM] ‎￼jesús‎: /home/public/libretube
        ‎[11:38:46 AM] ‎￼jesús‎: más:

        sudo chown you-user:http -R  /home/public

- Nota:

        ‎[11:41:20 AM] ‎￼jesús‎: «Entender qué ocurre en el O.S. te evita dolores de cabeza»


- Paso numero 8:

        [11:44:22 AM] ‎￼jesús‎: # PHP7 Support
        Include conf/extra/php7_module.conf
        ‎[11:51:55 AM] ‎￼jesús‎: Oh si, es necesario también :
        LoadModule php7_module /usr/libexec/httpd/modules/libphp7.so
        ‎[11:52:00 AM] ‎￼supersaravia‎: tuve que comentar la línea:
        #LoadModule mpm_event_module /usr/libexec/httpd/modules/mod_mpm_event.so
        y descomentar
        LoadModule mpm_prefork_module /usr/libexec/httpd/modules/mod_mpm_prefork.so

- Paso numero 9:

        [11:54:30 AM] ‎￼jesús‎: > y que en otra pc no resuelve la dirección a mi hyperpc libretube.test
        Eso es porque la otra pc no sabe en qué ip está libretube.test
        ‎[11:54:48 AM] ‎￼jesús‎: ^ eso se resuelve con:
        ‎[11:55:08 AM] ‎￼jesús‎: sudo nano -w /etc/hosts
        ‎[11:55:47 AM] ‎￼jesús‎: Your_IP_where_libretube       libretube.test
        ‎[11:56:00 AM] ‎￼jesús‎: example
        ‎[11:56:00 AM] ‎￼supersaravia‎: ya en la laptop receptor? ✓
        ‎[11:56:15 AM] ‎￼jesús‎: 192.168.1.56     libretube.test
        ‎[11:57:15 AM] ‎￼jesús‎: Desde los años 19XX se usaba /etc/hosts como si fuera un DNS
        ‎[11:57:49 AM] ‎￼jesús‎: En estos tiempos quien resuelve la IP de los sitios son los DNS
