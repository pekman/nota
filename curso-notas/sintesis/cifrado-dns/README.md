# browser --> dnsmasq --> dnscrypt-proxy --> gateway


## Add to /etc/dhcpcd.conf


    ```console
    # A hook script is provided to lookup the hostname if not set by DHCP
    # server, but it should not be run by default.
    # nohook lookup-hostname
    # nohook resolv.conf
    # Static domain
    static domain_name_servers=127.0.0.1 ::1
    ```


## Add to /etc/dnscrypt-proxy/dnscrypt-proxy.toml


    ```console
    listen_addresses = ['127.0.2.1:8053']
    ```


* ^ remplazar el 127.0.2.1 por 127.0.0.1 cuando se trata de maquinas virtuales


## Add to /etc/dnsmasq.conf


    ```console
    # Set config
    no-resolv
    server=127.0.2.1#8053
    #listen-address=127.0.0.1
    listen-address=::1,127.0.0.1
    # Set the cachesize here.
    # Defaults to 150
    cache-size=5000
    ```

* ^ remplazar el 127.0.2.1 por 127.0.0.1 cuando se trata de maquinas virtuales


## Restart services


    ```console
    sudo rc-service dhcpcd restart
    sudo rc-service dnscrypt-proxy restart
    sudo rc-service dnsmasq restart
    ```


## ^ Testing la configuracion


    ```console
    netstat -nl -4 -6`
    dig +short gnu.org @127.0.0.1`
    # Test from website:
    iceweasel-uxp https://www.dnsleaktest.com/`
    ```
