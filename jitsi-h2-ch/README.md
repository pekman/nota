
# Parte de código para remplazar en jitsi cuando se graba la pantalla


    <div style="background:rgba(0,0,0,.7);" class="toolbox-content-items">

        <p>Tesla & Twain:</p>
        <p>En sus corrientes informáticas.</p>

        <div style="display:none;">
            <div class="audio-preview">
                <div class="settings-button-container">
                    <div aria-pressed="false" aria-disabled="false" aria-label="Silenciar micrófono" class="toolbox-button" tabindex="0" role="button">
                        <div>
                            <div class="toolbox-icon   ">
                                <div class="jitsi-icon ">
                                    <svg height="22" width="22" viewBox="0 0 24 24">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M16 6a4 4 0 00-8 0v6a4.002 4.002 0 003.008 3.876c-.005.04-.008.082-.008.124v1.917A6.002 6.002 0 016 12a1 1 0 10-2 0 8.001 8.001 0 007 7.938V21a1 1 0 102 0v-1.062A8.001 8.001 0 0020 12a1 1 0 10-2 0 6.002 6.002 0 01-5 5.917V16c0-.042-.003-.083-.008-.124A4.002 4.002 0 0016 12V6zm-4-2a2 2 0 00-2 2v6a2 2 0 104 0V6a2 2 0 00-2-2z">
                                        </path>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div>
                            <div class="jitsi-icon settings-button-small-icon settings-button-small-icon--disabled">
                                <svg fill="none" height="9" width="9" viewBox="0 0 10 7">
                                    <path clip-rule="evenodd" d="M8.411 6.057A.833.833 0 109.65 4.943L5.73.563a.833.833 0 00-1.24 0L.63 4.943a.833.833 0 001.24 1.114l3.24-3.691L8.41 6.057z">
                                    </path>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="video-preview">
                <div class="settings-button-container">
                    <div aria-pressed="false" aria-disabled="false" aria-label="Alternar vídeo" class="toolbox-button" tabindex="0" role="button">
                        <div>
                            <div class="toolbox-icon   ">
                                <div class="jitsi-icon ">
                                    <svg fill="none" height="22" width="22" viewBox="0 0 22 22">
                                        <path clip-rule="evenodd" d="M13.75 5.5H3.667c-1.013 0-1.834.82-1.834 1.833v7.334c0 1.012.821 1.833 1.834 1.833H13.75c1.012 0 1.833-.82 1.833-1.833v-.786l3.212 1.835a.916.916 0 001.372-.796V7.08a.917.917 0 00-1.372-.796l-3.212 1.835v-.786c0-1.012-.82-1.833-1.833-1.833zm0 3.667v5.5H3.667V7.333H13.75v1.834zm4.583 4.174l-2.75-1.572v-1.538l2.75-1.572v4.682z">
                                        </path>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div>
                            <div class="jitsi-icon settings-button-small-icon">
                                <svg fill="none" height="9" width="9" viewBox="0 0 10 7">
                                    <path clip-rule="evenodd" d="M8.411 6.057A.833.833 0 109.65 4.943L5.73.563a.833.833 0 00-1.24 0L.63 4.943a.833.833 0 001.24 1.114l3.24-3.691L8.41 6.057z">
                                    </path>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div aria-label="Alternar pantalla compartida" class="toolbox-button" role="button" tabindex="0">
                <div>
                    <div class="toolbox-icon ">
                        <div class="jitsi-icon ">
                            <svg fill="none" height="22" width="22" viewBox="0 0 22 22">
                                <path clip-rule="evenodd" d="M3.667 2.75h14.666c1.013 0 1.834.82 1.834 1.833v11c0 1.013-.821 1.834-1.834 1.834h-2.75a.917.917 0 010 1.833H6.417a.917.917 0 010-1.833h-2.75a1.833 1.833 0 01-1.834-1.834v-11c0-1.012.821-1.833 1.834-1.833zm0 1.833v11h14.666v-11H3.667zm8.25 3.667c-3.75 0-5.5 1.604-5.5 6.417 2-3.896 5.5-3.667 5.5-3.667v1.274a.5.5 0 00.813.39l3.287-2.65a.5.5 0 000-.778l-3.287-2.65a.5.5 0 00-.813.39V8.25z">
                                </path>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
            <div class="toolbar-button-with-badge">
                <div aria-label="Alternar ventana de chat" aria-pressed="false" class="toolbox-button" role="button" tabindex="0">
                    <div>
                        <div class="toolbox-icon ">
                            <div class="jitsi-icon ">
                                <svg fill="none" height="22" width="22" viewBox="0 0 22 22">
                                    <path clip-rule="evenodd" d="M15.583 20.196c.507 0 .917-.41.917-.917v-4.612h2.75c.506 0 .917-.41.917-.917v-11a.917.917 0 00-.917-.917H2.75a.917.917 0 00-.917.917v11c0 .506.41.917.917.917h7.843l4.283 5.195a.917.917 0 00.707.334zM3.667 3.666h14.666v9.167h-3.666v3.893l-3.21-3.893h-7.79V3.667z">
                                    </path>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
                <span class="badge-round">
                    <span>
                    </span>
                </span>
            </div>
            <div aria-label="Levantar o bajar la mano" class="toolbox-button" role="button" tabindex="0">
                <div>
                    <div class="toolbox-icon ">
                        <div class="jitsi-icon ">
                            <svg fill="none" height="22" width="22" viewBox="0 0 22 22">
                                <path clip-rule="evenodd" d="M11.917 3.208v8.709a.917.917 0 001.833 0V4.833c0-.097.168-.25.458-.25.29 0 .459.153.459.25v7.084a.917.917 0 001.833 0V6.875a.458.458 0 11.917 0v6.913c-.25 2.552-2.425 4.545-5.042 4.545a5.037 5.037 0 01-4.205-2.258l-.003.001-3.495-5.255c-.168-.24-.117-.526.09-.671a.458.458 0 01.638.112l1.173 1.753h-.001a.917.917 0 001.676-.482h.002V5.043a.458.458 0 01.917-.001V11.918a.917.917 0 001.833 0V3.208a.458.458 0 11.917 0zm7.317 10.56l.016-.008V6.876a2.292 2.292 0 00-2.76-2.244c-.112-1.055-1.091-1.881-2.282-1.881-.17 0-.335.017-.495.049a2.292 2.292 0 00-4.51.004 2.292 2.292 0 00-2.786 2.238v3.663a2.293 2.293 0 00-3.27 3.135l3.302 4.94a6.875 6.875 0 0012.785-3.01z">
                                </path>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
            <div aria-pressed="false" aria-disabled="false" aria-label="Alternar vista de mosaico" class="toolbox-button" tabindex="0" role="button">
                <div>
                    <div class="toolbox-icon   ">
                        <div class="jitsi-icon ">
                            <svg height="22" width="22" viewBox="0 0 22 22">
                                <path d="M7.228 11.464H1.996c-.723 0-1.308.587-1.308 1.309v5.232c0 .722.585 1.308 1.308 1.308h5.232c.723 0 1.308-.586 1.308-1.308v-5.232a1.31 1.31 0 00-1.308-1.309zm0 5.887a.654.654 0 01-.654.654H2.649a.654.654 0 01-.654-.654v-3.924c0-.361.292-.654.654-.654h3.924c.361 0 .654.293.654.654v3.924zm10.464-5.887H12.46c-.723 0-1.308.587-1.308 1.309v5.232c0 .722.585 1.308 1.308 1.308h5.232c.722 0 1.308-.586 1.308-1.308v-5.232a1.31 1.31 0 00-1.308-1.309zm0 5.887a.654.654 0 01-.654.654h-3.924a.654.654 0 01-.654-.654v-3.924c0-.361.293-.654.654-.654h3.924c.361 0 .654.293.654.654v3.924zM7.228 1H1.996C1.273 1 .688 1.585.688 2.308V7.54c0 .723.585 1.308 1.308 1.308h5.232c.723 0 1.308-.585 1.308-1.308V2.308C8.536 1.585 7.95 1 7.228 1zm0 5.886a.654.654 0 01-.654.654H2.649a.654.654 0 01-.654-.654V2.962c0-.361.292-.654.654-.654h3.924c.361 0 .654.292.654.654v3.924zM17.692 1H12.46c-.723 0-1.308.585-1.308 1.308V7.54c0 .723.585 1.308 1.308 1.308h5.232c.722 0 1.308-.585 1.308-1.308V2.308C19 1.585 18.414 1 17.692 1zm0 5.886a.654.654 0 01-.654.654h-3.924a.654.654 0 01-.654-.654V2.962c0-.361.293-.654.654-.654h3.924c.361 0 .654.292.654.654v3.924z">
                                </path>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
            <div aria-label="Invitar personas" class="toolbox-button" role="button" tabindex="0">
                <div>
                    <div class="toolbox-icon ">
                        <div class="jitsi-icon ">
                            <svg fill="none" height="22" width="22" viewBox="0 0 24 24">
                                <path clip-rule="evenodd" d="M14 7a2 2 0 11-4 0 2 2 0 014 0zM8 7a4 4 0 108 0 4 4 0 00-8 0zm4 5c-6.014 0-8 2.25-8 6.75 0 1.5.667 2.25 2 2.25h8.758a4.5 4.5 0 103.812-7c-1.216-1.333-3.296-2-6.57-2zm4.164 2.653C15.236 14.191 13.894 14 12 14c-4.698 0-6 1.174-6 4.75 0 .11.004.191.007.25h8.02a4.498 4.498 0 012.136-4.347zM19 18h2v1h-2v2h-1v-2h-2v-1h2v-2h1v2z">
                                </path>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
            <div class="toolbox-button-wth-dialog">
                <div aria-label="Alternar más acciones" aria-pressed="false" class="toolbox-button" role="button" tabindex="0">
                    <div>
                        <div class="toolbox-icon ">
                            <div class="jitsi-icon ">
                                <svg fill="none" height="22" width="22" viewBox="0 0 22 22">
                                    <path clip-rule="evenodd" d="M6.417 11a1.833 1.833 0 11-3.667 0 1.833 1.833 0 013.667 0zM11 12.833a1.833 1.833 0 100-3.666 1.833 1.833 0 000 3.666zm6.417 0a1.833 1.833 0 100-3.666 1.833 1.833 0 000 3.666z">
                                    </path>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div aria-disabled="false" aria-label="Colgar" class="toolbox-button" tabindex="0" role="button">
                <div>
                    <div class="toolbox-icon   hangup-button">
                        <div class="jitsi-icon ">
                            <svg height="22" width="22" viewBox="0 0 32 32">
                                <path d="M16 12c-2.125 0-4.188.313-6.125.938v4.125c0 .5-.313 1.063-.75 1.25a13.87 13.87 0 00-3.563 2.438c-.25.25-.563.375-.938.375s-.688-.125-.938-.375L.373 17.438c-.25-.25-.375-.563-.375-.938s.125-.688.375-.938c4.063-3.875 9.563-6.25 15.625-6.25s11.563 2.375 15.625 6.25c.25.25.375.563.375.938s-.125.688-.375.938l-3.313 3.313c-.25.25-.563.375-.938.375s-.688-.125-.938-.375a13.87 13.87 0 00-3.563-2.438c-.438-.188-.75-.625-.75-1.188V13c-1.938-.625-4-1-6.125-1z">
                                </path>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


- Usa `v` para apagar la camara
- Usa `d` para compartir pantalla o volver a la camara
