# Default touchpad libinput


- Para configurar el touchpad sin gesturas, que permite trabajar con
las manos descanzadas en la laptop sin incomodar el touchpad


## vefiricar xorg.conf


`man xorg.conf`


`sudo emacs /etc/xorg.conf`

### Explicacion:

- ^ configuración disponible para los usuarios según el `man
  xorg.conf`.

- verificar el codigo del touch pad `xinput list`.

- en mi caso el es `11`.

- listar para verificar ¿cual el controlador que está manipulando el
  touchpad?.

- puedo ver que en mi caso es `libinput`.

- agrego esta conf sacada de
  `/usr/share/X11/xorg.conf.d/40-libinput.conf`.

- ^ por el enunciado en el `man xorg.conf`.


         Finally, configuration files will also be searched for in a
         directory reserved for system use.  This is to separate
         configuration files from the vendor or 3rd party packages from
         those of local administration.  These files are found in the
         following directory:

         /usr/share/X11/xorg.conf.d

- entonces investigo y veo que puedo setear para mi conveniencia:

- Ingresamos este archivo aquí `sudo emacs /etc/xorg.conf`

        Section "InputClass"
            Identifier "libinput touchpad catchall"
            MatchIsTouchpad "on"
            MatchDevicePath "/dev/input/event*"
            Driver "libinput"
            Option "ClickMethod" "clickfinger"
            Option "HorizontalScrolling" "false"
            Option "LeftHanded" "false"
            Option "MiddleEmulation" "false"
            Option "NaturalScrolling" "false"
            Option "ScrollMethod" "none"
            Option "Tapping" "false"
            Option "TappingDrag" "false"
        EndSection

- en donde intento deshabilitar todo gesto adicional a lo que es,
  clic, clic derecho y dirigir el cursor.

- y en mi caso debo añadir a mi usuario al group input

- ^ con el siguiente comando `sudo gpasswd -a user input`

- finalizada la configuración con libinput
