# Cómo aumentar el tamaño de la tipografía en tty
#### how-to - how2 - cli - clih2 - h2

- encontrar la tipografia deseada en

    /usr/share/kbd/consolefonts

- por ejemplo yo tengo estas haciendo un ls

        partialfonts/                 koi8r-8x8.gz
        161.cp.gz                     koi8r.8x8.psfu.gz
        162.cp.gz                     koi8u_8x14.psfu.gz
        163.cp.gz                     koi8u_8x16.psfu.gz
        164.cp.gz                     koi8u_8x8.psfu.gz
        165.cp.gz                     lat0-08.psfu.gz
        737.cp.gz                     lat0-10.psfu.gz
        880.cp.gz                     lat0-12.psfu.gz
        928.cp.gz                     lat0-14.psfu.gz
        972.cp.gz                     lat0-16.psfu.gz
        Agafari-12.psfu.gz            lat0-sun16.psfu.gz
        Agafari-14.psfu.gz            lat1-08.psfu.gz
        Agafari-16.psfu.gz            lat1-10.psfu.gz
        alt-8x14.gz                   lat1-12.psfu.gz
        alt-8x16.gz                   lat1-14.psfu.gz
        alt-8x8.gz                    lat1-16.psfu.gz
        altc-8x16.gz                  lat2-08.psfu.gz
        aply16.psf.gz                 lat2-10.psfu.gz
        arm8.fnt.gz                   lat2-12.psfu.gz
        cp1250.psfu.gz                lat2-14.psfu.gz
        cp850-8x14.psfu.gz            lat2-16.psfu.gz
        cp850-8x16.psfu.gz            lat2a-16.psfu.gz
        cp850-8x8.psfu.gz             lat2-sun16.psfu.gz
        cp857.08.gz                   Lat2-Terminus16.psfu.gz
        cp857.14.gz                   lat4-08.psfu.gz
        cp857.16.gz                   lat4-10.psfu.gz
        cp865-8x14.psfu.gz            lat4-12.psfu.gz
        cp865-8x16.psfu.gz            lat4-14.psfu.gz
        cp865-8x8.psfu.gz             lat4-16.psfu.gz
        cp866-8x14.psf.gz             lat4-16+.psfu.gz
        cp866-8x16.psf.gz             lat4-19.psfu.gz
        cp866-8x8.psf.gz              lat4a-08.psfu.gz
        cybercafe.fnt.gz              lat4a-10.psfu.gz
        Cyr_a8x14.psfu.gz             lat4a-12.psfu.gz
        Cyr_a8x16.psfu.gz             lat4a-14.psfu.gz
        Cyr_a8x8.psfu.gz              lat4a-16.psfu.gz
        cyr-sun16.psfu.gz             lat4a-16+.psfu.gz
        default8x16.psfu.gz           lat4a-19.psfu.gz
        default8x9.psfu.gz            lat5-12.psfu.gz
        drdos8x14.psfu.gz             lat5-14.psfu.gz
        drdos8x16.psfu.gz             lat5-16.psfu.gz
        drdos8x6.psfu.gz              lat7-14.psfu.gz
        drdos8x8.psfu.gz              lat7a-14.psfu.gz
        ERRORS                        lat7a-16.psf.gz
        eurlatgr.psfu.gz              lat9-08.psf.gz
        Goha-12.psfu.gz               lat9-10.psf.gz
        Goha-14.psfu.gz               lat9-12.psf.gz
        Goha-16.psfu.gz               lat9-14.psf.gz
        GohaClassic-12.psfu.gz        lat9-16.psf.gz
        GohaClassic-14.psfu.gz        lat9u-08.psfu.gz
        GohaClassic-16.psfu.gz        lat9u-10.psfu.gz
        gr737a-8x8.psfu.gz            lat9u-12.psfu.gz
        gr737a-9x14.psfu.gz           lat9u-14.psfu.gz
        gr737a-9x16.psfu.gz           lat9u-16.psfu.gz
        gr737b-8x11.psfu.gz           lat9v-08.psfu.gz
        gr737b-9x16-medieval.psfu.gz  lat9v-10.psfu.gz
        gr737c-8x14.psfu.gz           lat9v-12.psfu.gz
        gr737c-8x16.psfu.gz           lat9v-14.psfu.gz
        gr737c-8x6.psfu.gz            lat9v-16.psfu.gz
        gr737c-8x7.psfu.gz            lat9w-08.psfu.gz
        gr737c-8x8.psfu.gz            lat9w-10.psfu.gz
        gr737d-8x16.psfu.gz           lat9w-12.psfu.gz
        gr928-8x16-thin.psfu.gz       lat9w-14.psfu.gz
        gr928-9x14.psfu.gz            lat9w-16.psfu.gz
        gr928-9x16.psfu.gz            LatArCyrHeb-08.psfu.gz
        gr928a-8x14.psfu.gz           LatArCyrHeb-14.psfu.gz
        gr928a-8x16.psfu.gz           LatArCyrHeb-16.psfu.gz
        gr928b-8x14.psfu.gz           LatArCyrHeb-16+.psfu.gz
        gr928b-8x16.psfu.gz           LatArCyrHeb-19.psfu.gz
        greek-polytonic.psfu.gz       latarcyrheb-sun16.psfu.gz
        iso01.08.gz                   latarcyrheb-sun32.psfu.gz
        iso01-12x22.psfu.gz           LatGrkCyr-12x22.psfu.gz
        iso01.14.gz                   LatGrkCyr-8x16.psfu.gz
        iso01.16.gz                   LatKaCyrHeb-14.psfu.gz
        iso02.08.gz                   Mik_8x16.gz
        iso02-12x22.psfu.gz           pancyrillic.f16.psfu.gz
        iso02.14.gz                   README.12x22
        iso02.16.gz                   README.Arabic
        iso03.08.gz                   README.cp1250
        iso03.14.gz                   README.cybercafe
        iso03.16.gz                   README.Cyrillic
        iso04.08.gz                   README.drdos
        iso04.14.gz                   README.Ethiopic
        iso04.16.gz                   README.eurlatgr
        iso05.08.gz                   README.eurlatgr.mappings
        iso05.14.gz                   README.Greek
        iso05.16.gz                   README.Hebrew
        iso06.08.gz                   README.lat0
        iso06.14.gz                   README.Lat2-Terminus16
        iso06.16.gz                   README.lat7
        iso07.14.gz                   README.lat9
        iso07.16.gz                   README.LatGrkCyr
        iso07u-16.psfu.gz             README.psfu
        iso08.08.gz                   README.Sun
        iso08.14.gz                   ruscii_8x16.psfu.gz
        iso08.16.gz                   ruscii_8x8.psfu.gz
        iso09.08.gz                   sun12x22.psfu.gz
        iso09.14.gz                   t850b.fnt.gz
        iso09.16.gz                   tcvn8x16.psf.gz
        iso10.08.gz                   t.fnt.gz
        iso10.14.gz                   UniCyr_8x14.psf.gz
        iso10.16.gz                   UniCyr_8x16.psf.gz
        koi8-14.psf.gz                UniCyr_8x8.psf.gz
        koi8c-8x16.gz                 UniCyrExt_8x16.psf.gz
        koi8r-8x14.gz                 viscii10-8x16.psfu.gz
        koi8r-8x16.gz

- para probar estas ^ tipografías debo indicarla en el archivo

         /etc/conf.d/consolefont

- remplazando la que tenemos por defecto

         consolefont="default12x20"

- ^ una vez remplazamos en modo tty (imagino que ves esto
  en tty)

- ejecutamos `doas service consolefont start`

- al elegir otra, ejecutamos `doas service consolefont restart`

- concluida la búsqueda podemos habilitar la nueva tipografia
  en el inicio del sistema con el comando

        doas rc-update add consolefont boot


- nota: si la tipografía no funciona, no dejes activado consolefont en los run levels

- pd: a mí me sentó bien "latarcyrheb-sun32.psfu.gz"
- me recuerda al nintendo que venia en teclado/casset de los años 2000
