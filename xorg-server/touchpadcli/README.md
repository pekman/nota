    Section "InputClass"
         Identifier "touchpad catchall"
         Driver "synaptics"
         MatchIsTouchpad "on"
         # This option is recommend on all Linux systems using evdev, but cannot be
         # enabled by default. See the following link for details:
         Option "ClickPad" "True"
         Option "EmulateMidButtonTime" "0"
         Option "TapButton1" "0"
         Option "TapButton2" "0"
         Option "TapButton3" "0"
         Option "RBCornerButtom" "0"
         Option "RTCornerButton" "0"
         Option "VertEdgeScroll" "off"
         Option "VertTwoFingerScroll" "off"
         Option "HorizEdgeScroll" "off"
         Option "HorizTwoFingerScroll" "off"
         # http://who-t.blogspot.com/2010/11/how-to-ignore-configuration-errors.html
         MatchDevicePath "/dev/input/event*"
    EndSection



    Section "InputDevice"
        Identifier "touchpad"
        Driver "synaptics"
        Option "ClickPad" "0"
        Option "EmulateMidButtonTime" "0"
        Option "TapButton1" "0"
        Option "TapButton2" "0"
        Option "TapButton3" "0"
        Option "TappingDrag" "0"
        Option "RBCornerButtom" "0"
        Option "RTCornerButton" "0"
        Option "VertEdgeScroll" "0"
        Option "VertTwoFingerScroll" "0"
        Option "HorizEdgeScroll" "0"
        Option "HorizTwoFingerScroll" "0"
    EndSection
