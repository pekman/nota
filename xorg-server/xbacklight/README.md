# Arreglar el botón para que incremente y disminuya la luz de la pantalla

- en el archivo `/usr/share/X11/xorg.conf.d/20-intel-brightness.conf`
- agregar lo siguiente:

        Section "Device"
                Identifier      "Card0"
                Driver          "intel"
                Option          "Backlight"     "/sys/class/backlight/intel_backlight"
        EndSection

- end
