# No Suspender la Pantalla al Apagar la Tapa

activar estas opciones así, en el archivo

/etc/elogind/logind.conf

    HandleLidSwitch=Switch off display
    HandleLidSwitchDocked=Switch off display
    SuspendKeyIgnoreInhibited=no
    HibernateKeyIgnoreInhibited=no
    LidSwitchIgnoreInhibited=yes
