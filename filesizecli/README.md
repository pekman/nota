# Ver Tamaños de Archivos

## File Size in MB

    ls -l --b=M  filename | cut -d " " -f5

## File Size in GB

    ls -l --b=G  filename | cut -d " " -f5

## Con unos lindos indicadores de espacio en el disco duro

    sudo pacman -S ncdu

    ncdu [path]

## Y comando ls genial

    ls -lrtsh

## Pintar tamaño de archivos *.txt ordenado por tamaño

    du -hs *.txt | sort -h
