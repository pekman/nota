# Renovar certificados en sistema operativo `hyperbola`


- En `/etc/pacman.conf` quitar comentario `#XferCommand = /usr/bin/curl -C - -f %u > %o`


- Agregar temporalmente la -k para actualizar certificados


- `XferCommand = /usr/bin/curl -k -C - -f %u > %o`


- `doas pacman -Syu`


- finalmente comentar la línea `#XferCommand = /usr/bin/curl -C - -f
  %u > %o` a como estaba
