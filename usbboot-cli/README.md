# Hacer una live usb de una distro GNU

## descargar una iso


- las distros GNU tienen formato `gnu-distro-example.iso` que el .iso emula un disco; es el instalador del sistema


- las isos están en la web oficial de la distro en la seccion de descargas y la descarga es un archivo.iso literalmente de hasta 2GB de tamaño dependiendo de la distro


- una vez descargado en la carpeta de descarga damos clic derecho abrir terminal


## en la terminal


- ponemos el usb (no importa si está lleno pero todo se perderá)


- una vez puesto el usb vamos a saber su ubicación con el comando `lsblk` <-- escribir ese comando en la terminal que abrimos y veremos algo como esto


```
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:16   0 465.8G  0 disk
├─sda1   8:17   0   512M  0 part /boot/efi
└─sda2   8:18   0 465.3G  0 part /
sdb      8:0    0     2G  0 disk
└─sdb1   8:1    0     1G  0 part

```


- ^ aqui me está indicando los discos que están conectados a mi sistema operativo GNU


- el que dice `sda1` es --> `/boot/efi` de 512M es el arrancador (GRUB)

- el `sda2` es el disco donde está todo desde mi sistema hasta mi home en mi caso

- IMPORTANTE: el `sdb` es el usb, entonces ya sabemos que `sdb` es mi usb por que mi usb es de 2GB

## grabar la iso

- el siguiente comando graba lo que está en la iso en el usb y se puede autocompletar con tabs por si el nombre de la iso es laguísimo

- `dd if=/dev/sdb of=gnu-distro-name-file.iso bs=1M status=progress`

- ^ aquí esperaremos a que grabe el disco nos va indicar el progreso de la grabación hasta terminar
