## optipng

### Installation

    pacman -S optipng

### Usage

Single compress:

    optipng -k -o7 file.png

Full compress:

    optipng -k -o7 -strip all file.png

All Files Compress:

    find -type f -name "*.png" -exec optipng -k -o7 -strip all {} \;

## jpegoptim

### Installation

    pacman -S jpegoptim

### Usage

Single compress:

    find /path/to/imagenes/ -type f -iname "*.jp*" -exec jpegoptim --strip-all {} \;
    jpegoptim --size=70k file.jpg

Full compress:

    jpegoptim --strip-all file.jpg

Custom compress:

    jpegoptim -S 100k file.jpg

## Delete Metadata to Images

agregando una copia_orginal.jpg:

    exiftool -all= *.jpg

replazando el archivo original:

    exiftool -overwrite_original -all= *.jpg

### Requeriments

    pacman -S mat perl-image-exiftool mutagen

### Resize all images

Full rezise 250 all files jpg:

    for file in *.jpg; do convert $file -resize x250 250-$file; done

### Remove space images

remover todos los espacios de las imagenes:

    for file in *.png; do mv "$file" "${file//[[:space:]]}"; done

remplazar espacios con guiones:

    for file in *; do mv "$file" "${file// /-}"; done


### Compress group image

comprimir cantidades de fotos

    for file in *.jpg; do jpegoptim -S 200k $file; done

## Grupo de IMG to One.PDF

convertir imagenes*.jpg a imagenes*.pdf

    for name in *.jpg; do convert $name `basename $name .png`.pdf; done

unir todos las imagenes*.jpg.pdf en One.PDF

    convert *.pdf ONE.pdf

# GIF

# extraer fotogramas

    convert image.gif -adjoin results.png

# colorear de un color una imagen

    convert phone.png -fill "#de2a2a" -colorize 100 phone-red.png

varias imagenes en una carpeta

    for file in *.png; do convert $file -fill "#de2a2a" -colorize 100 coloreado-$file; done

# copy image by command line

    xclip -selection clipboard -t image/png -i saravia_avatar.png
