# Unir videos

Ejemplo de script para unir 2 o más videos


    #!/bin/bash
    ls parte_1.mp4 parte_2.mp4 | \
    perl -ne 'print "file $_"' | \
    ffmpeg -protocol_whitelist file,http,https,tcp,tls,crypto,pipe  -f concat -i - -c copy unido.mp4

# Cortar vídeos al vuelo

### mkv no soportado

- `ffmpeg -i video.webm -ss 00:05:00 -t 00:07:00 -c copy nombre_final.webm`
