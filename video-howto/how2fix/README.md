# CORREGIR AUDIO DE VIDEO


## extraer audio

    ffmpeg -i videofile.mp4 -vn -acodec copy audiofile.mp3
    ### https://gist.github.com/protrolium/e0dbd4bb0f1a396fcb55

### Trabajar con la guia de audio...

## remover audio

    ffmpeg -i input_file.mp4 -vcodec copy -an output_file.mp4
    ###https://unix.stackexchange.com/a/33864

## agregar audio corregido al video

    ffmpeg -i video.mp4 -i audio.aac -map 0:0 -map 1:0 -vcodec copy -acodec copy newvideo.mp4
    ###https://askubuntu.com/questions/369947/how-to-add-an-audio-file-aac-or-mp3-to-a-mp4-video-file
