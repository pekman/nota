## how to install NPM with Python Pip like a package manager

### Pre-requisites

    sudo pacman -S python-virtualenv

### Make virtual venv only for install NPM with Python Pip

    virtualenv -p python3 venv

    activate venv

    pip install nodeenv

### Make NPM nodejs(env) for activate npm on terminal

    nodeenv --list

    nodeenv --node=8.10.0 --npm=6.13.7 --source --jobs=4 nodejs

### Greatest Notice

    only go or in to the instalation folder and run:

    activate nodejs

    ^ now you can use npm nodejs folder version
