## Cambiar permisos recursivamente solo a directorios:

    find . -type d -exec chmod -R 0755 {} \;

## Cambiar permisos recursivamente solo a archivos:

    find . -type f -exec chmod -R 0644 {} \;

# Configurar permisos en el servidor Hyperbola

Esta es la forma correcta de configurar los permisos para el servidor GNU
$USER debe ser reemplazado por el usuario que va a tener acceso al servidor

sudo gpasswd -a heckyel http

sudo chown http:http -R /srv/http/project

sudo chmod g+w -R /srv/http/project

sudo chmod ag+s /srv/http/project

umask 0002