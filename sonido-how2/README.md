# TRABAJO CON SONIDO POR COMANDOS

## Para poner guiones bajos a todos los archivos de un tiron

    for f in *\ *; do mv "$f" "${f// /_}"; done

## Para quitar un patron de texto a todos los archivo de un tiron

Ejemplo loud_mi_cancion.mp3 y resulte mi_cancion.mp3

    for f in *.mp3; do mv "$f" "${f//loud_/}"; done


## Para limpiar ruidos

## Instalar `sudo apt install sox libsox-fmt-*` ×D

Obtener la muestra de ruido

    ffmpeg -i source.mp3 -vn -ss 00:00:18 -t 00:00:20 noisesample.wav

Generear los Objetos del Ruido

    sox noisesample.wav -n noiseprof noise_profile_file

Aplicar la reduccion de ruido

    sox source.mp3 output.mp3 noisered noise_profile_file 0.31

Reducir ruido recursivamente una ves configurada

    for file in *.mp3; do sox $file noise_$file noisered noise_profile_file 0.31; done


fuente https://unix.stackexchange.com/a/427343

## Para subir el volumen a varios audios de un tiron

se necesita nombres de archivos sin espacios...

Varios archivos de un tiron

    for file in *.mp3; do lame --scale 4 $file loud_$file; done

uno por uno

    lame --scale 4 source.mp3 loud_source.mp3

## Portada

Agregar portada a un archivo de audio de un tiron o tambien uno por uno

    eyeD3 --add-image "cover.png:FRONT_COVER" *.mp3

Quitar portada a un archivo de audio de un tiron o tambien de uno por uno

    eyeD3 --add-image ":FRONT_COVER" *.mp3

## Extraer todos los Audios de todos los archivos mp4 de un tiron

    for file in *.mp4; do ffmpeg -i $file $file.mp3; done

## Remover

Para remover todos los archivos que contengan un patron, por ejemplo loud_

    find . -iname "loud_*" -type f -exec rm -v {} \;

Para remover todos los archivos que ¡NO! contengan un patron, por ejemplo loud_

    find . ! -iname "loud_*" -type f -exec rm -v {} \;
