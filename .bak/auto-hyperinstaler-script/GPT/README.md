pacman -S base-devel grub efibootmgr dosfstools openssh os-prober mtools linux-libre-lts-api-headers linux-libre-lts wpa_supplicant iw ldns xenocara-input-synaptics

echo LANG=es_PE.UTF-8 > /etc/locale.conf

locale-gen
