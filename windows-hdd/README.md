# CÓMO MONTAR UN DISCO QUE PROVIENE DE WINDOWS

- ir al home

`cd`

- generar la carpeta

`mkdir WindowsPartition`

- gestionar los permisos correctamente "no necesitas remplazar `$USER`" ;)

`sudo chown $USER -R ~/WindowsPartition`

- mirar el dispositivo intoxicado con windows conectado

`lsblk`

- en mi caso es el `c`(`/dev/sdX`) `/dev/sdc1` por que tengo 3 discos `a,b,c`

- fixear el disco ntfs

`ntfsfix /dev/sdc1`

- instalar dependencias

`sudo pacman -S ntfs-3g fuse`

- montar con `ntfs-3g` como sudo

`sudo ntfs-3g /dev/sdc1 ~/WindowsPartition`

- en mi caso escribí una carpeta en mi home quizá eso me ayuda a entrar al disco
