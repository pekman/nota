# Hyperbla Instalación Síntesis


## Comandos básicos de instalación del nuevo kernel


`sudo pacman -U linux-libre-hyperbola-5.4.43-1-x86_64.pkg.tar.xz linux-libre-hyperbola-headers-5.4.43-1-x86_64.pkg.tar.xz`


`sudo mkinitcpio -p linux-libre-hyperbola`


`sudo grub-mkconfig -o /boot/grub/grub.cfg`


## custom grub-install


`sudo grub-install --target=x86_64-efi --bootloader-id=GRUB --recheck --efi-directory=/boot/efi`


## lvmatad


- Descubrimos que si no tenermos encendido para discos sólidos lvmatad nesecitamos antes tener encendido el servicio, para que instale el kernel sin ningun problema


`sudo rc-update add lvmatad`


`sudo service lvmatad start`


## tree


- revisamos el arbol para trabajar en el grub, ya que en este ejemplo, se hizo la instalación en una ASUS X205TA que necesita el bootia32.efi para encender


        /boot/efi
        ├── EFI
        │   ├── BOOT
        │   │   ├── BOOTIA32.EFI
        │   │   └── BOOTX64.EFI
        │   └── GRUB
        │       ├── bootia32.efi
        │       └── grubx64.efi
        └── startup.nsh


## grub


- Hay que decirle a bootia32 que coja la configuración del grub.


`sudo grub-mkstandalone -d /usr/lib/grub/i386-efi/ -O i386-efi --modules="part_gpt part_msdos" --fonts="unicode" --locales="uk" --themes="" -o "/boot/efi/EFI/GRUB/bootia32.efi" "boot/grub/grub.cfg=/boot/grub/grub.cfg" -v`


## cp


- hacemos una copia que es el lugar donde ovedece esta laptop para arrancar hyperbola xD!!


`sudo cp -v /boot/efi/EFI/GRUB/bootia32.efi /boot/efi/EFI/BOOT/BOOTIA32.EFI`


## Sound Cards


- revisar las placas de sonido


`cat /proc/asound/cards`



## dsmeg


`dmesg >> $HOME/dmesg.log.txt`


- Aqui se ve que no se cargó el controlador de audio en linux-libre-lts por ser privativo, a pesar de tener el ultimo kernel-firmware compilado desde https://codeberg.org/heckyel/packages


        [  132.007612] 80860F28:00: Missing Free firmware (non-Free firmware loading is disabled)
        «El id 80860F28:00 es Intel SST Audio Device (WDM)»

        [    9.372577] rt5645 i2c-10EC5648:00: i2c-10EC5648:00 supply avdd not found, using dummy regulator
        [    9.372580] [drm] HDaudio controller not detected, using LPE audio instead
