### Activate virtualenv for python

    virtualenv -p python3 venv

    source venv/bin/activate

    pip install virtualenvwrapper.npm

### NPM on vitualenv

    source venv/bin/virtualenvwrapper.sh

    mkvirtualenv npmvirtual

    workon npmvirtual


### Test app NPM on virtualenv

    npm install -g gulp

### Delete NPM virtualenvs

    rm -rv /home/$USER/.virtualenvs/

# FOR WHITOUT NODEJS AND NPM IN HYPERBOLA REPOSITORIES

## NPM in Python virtualenv

### Pre-requisites

    sudo pacman -S python-virtualenv

### Activate virtualenv for python

    virtualenv -p python3 venv

    source venv/bin/activate

    pip install nodeenv

### NPM on vitualenv

    nodeenv nodejsenv

    source nodejsenv/bin/activate

### Test app NPM on virtualenv

    node -v

    npm -v

    npm install -g gulp

### Deactivate environment NodeJS

    deactivate_node

### Deactivate environment Python

    deactivate

### Delete all (NPM and Python) virtualenvs

    rm -rv .venv/ .nodejsenv/
