# NPM
### whitout sudo dummmy way

`mkdir ~/.npm-global`

## Configure npm to use the new directory path:

`npm config set prefix '~/.npm-global'`

## In your preferred text editor, open or create a

`~/.bashrc`

## file and add this line:

`export PATH=~/.npm-global/bin:$PATH`
