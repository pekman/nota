# Exploración de GNU Guile como programador Web y Emacsen

## Contexto:

Exploraré a ver como puedo iniciarme en Scheme, ya que lo recomienda
el mismo rms[0] fundador del proyecto GNU, que deberíamos aprender
alguna variante de Scheme, por que cuando lo aprendamos, nos daremos
cuenta de qué carecen los demás lenguajes de programación y que tiene
cosas que no imaginan los desarrolladores de otros lenguajes.

## Suerte:

Entonces lo primero que buscaría sería algo referente a usar en emacs;
como un emacsen, primero ejecutaré lo siguiente `mupdf -I
guile.pdf`[1], donde buscaré con `/` + `emacs`; donde me topo con una
introducción para emacsens, super simplificada en pocas palabras 2
opciones que seguir, y en la segunda[2] le di al clavo:

## Allanando el terreno:

Resumiendo encontré que para emacs debería teclear `M-x`
`package-install` seguido de `geiser-guile` esto me habilitaría una
consola al estilo terminal bash, sólo que en vez de bash, tendría
guile, a esto le dicen REPL y sirve para hacer especie de testeos e
inclusive parece que se podría usar como el mismo bash, bueno, no me
extendí mucho en probarlo, tan sólo con ver que ya tenia el pie
derecho delante, volví al `mupdf -I guile.pdf` y tecleé: `g`, `/` +
`web service` y le volví a dar al clavo[4]: encontré varios "Hello
world!" fáciles de teclear: en mi caso, ya que estuve en el `pdf` no
pude visualizar la diferencia de comilla simple y comilla invertida
entonces después de un buen rato de revisar el último ejemplo de `hola
mundo` de casualidad casi, fuí a la versión web, que es la que
comparto en los enlaces, y `buenazo` sólo era la comilla invertida, me
salía un error de `unqoted` al armar el `string` de `templatize`.

Les dejo el `script` que hace el último ejemplo de `web service` del
GNU Guile para que los meses y intentos fallidos que hice alguna vez
no lo repita quizá otro hispanohablante, después de leer esto, podrías
replicar estos pasos para comodidad y poder continuar la aventura
técnica Scheme que nos promete RMS.

## El script mas largo funcionó:

    (use-modules (web server)
                 (web request)
                 (web response)
                 (sxml simple))

    (define (templatize title body)
      `(html (head (title ,title))
             (body ,@body)))

    (sxml->xml (templatize "Hello!" '((b "Hi!"))))

    (define* (respond #:optional body #:key
                      (status 200)
                      (title "Hello hello!")
                      (doctype "<!DOCTYPE html>\n")
                      (content-type-params '((charset . "utf-8")))
                      (content-type 'text/html)
                      (extra-headers '())
                      (sxml (and body (templatize title body))))
      (values (build-response
               #:code status
               #:headers `((content-type
                            . (,content-type ,@content-type-params))
                           ,@extra-headers))
              (lambda (port)
                (if sxml
                    (begin
                      (if doctype (display doctype port))
                      (sxml->xml sxml port))))))


    (define (debug-page request body)
      (respond
       `((h1 "hello world!")
         (table
          (tr (th "header") (th "value"))
          ,@(map (lambda (pair)
                   `(tr (td (tt ,(with-output-to-string
                                   (lambda () (display (car pair))))))
                        (td (tt ,(with-output-to-string
                                   (lambda ()
                                     (write (cdr pair))))))))
                 (request-headers request))))))

    (run-server debug-page)
