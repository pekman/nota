# Reflexiones que conectarán curiosidad por el software libre


Siempre quise escribir un articulo para CL, pero casi siempre me
empujaba a mi mismo a añadir algo de software privativo, como suele
pasar, me imagino que es lo mas normal que les pasa a todos para
tratar de «atraer público», pero me dije a mi mismo -me considero un
usuario de GNU bien portado, en una distro 100% libre, creo que ya
puedo decir algo interesante que no se ensucie con software privativo-
entonces, de suerte, vi un vídeo de pocos minutos con científicos
argentinos del software libre, cuando terminaron los 12 minutos mas o
menos, quedé fascinado por los términos -Informática generativa- y
-Computadora de uso general- empotrando y conectando cables en mi
cabeza para encontrar la forma de -agregarlo en mis conversaciones,
familiares o en el trabajo- -fomentaría la ciencia- -fomentaría algo
que suma- -fomentaría conocimiento- pero decir todo esto crudamente no
me ha funcionado hasta ahora, sólo he logrado espantar a la gente,
entonces, después de seguir investigando, me sugerí a mi mismo unas 3
geniales reflexiones, para conectar a mis amigos, a mis familiares con
la ciencia mostrando que "el software libre es un tema fascinante" sin
espantarlos.


### Conexión 1:


## El Bienestar


Puedes compartir tu discurso del software libre; explicando el, cómo
la sociedad ha construido su bienestar, como cuando se descubrió la
electricidad y al día de hoy, la familia para bien, está compartiendo
tiempo de calidad en sus hogares.


Pues cómo motivaremos al joven Tesla o al joven Edison que despeguen
su curiosidad, si les decimos: "Lo que estás preguntando es un secreto
y no lo puede saber nadie, si lo descubres irás a la cárcel".


^ pues, es esto es real al día de hoy y pasa seguido en las clases de
informática, siendo esta la primera estrategia de conexión que les
agrada a todos y despierta la curiosidad.


### Conexión 2:


## El Buen Samaritano


##### Ayudar al prójimo


Fomentar con nuestras palabras, que compartir software libre esta
genial, y no necesitas un permiso legal para regalar o vender tus
copias, a diferencia del software privativo, o que puedes instalar tus
propios servicios, que sólo encendiendo tu laptop puedes hacerles uso.
lo que está pasando ahora es que tus mejores amigos te están diciendo
indirectamente: "es software de paga, y es así por que lo bueno cuesta
caro, y si no pagas tú, no lo tendrás", es un pensamiento aceptado
entre la mayoría hoy en día.


### Conexión 3:


## El Bien Sembrar


##### La informática generativa no es un sueño, es la hermana del software libre.


La historia lo dice sólo, se logró la distro Slackware GNU+linux-libre
en el año 1992 el primer sistema operativo 100% libre, Sembrado por
Dr. Richard Stallman en 1983 junto a centenas de programadores de al
rededor del mundo, que «generó» al día de hoy, para bien
«distribuciones 100% libres» y para no tan mal «distribuciones semi
libres», por lo mismo esto se conoce como, tecnología, por que «genera
más conocimiento» «genera bienestar», fomenta «ayuda al prójimo» y
muestra sus conocimientos al público, analógicamente esto es software
libre «informática generativa», ya que se debe de separar de la
«informática destructiva» software privativo, que es la que niega el
conocimiento y se vuelve el patrón de tu laptop, entonces, para el
bien de todos, si por ahí vez una niña o niño muy genial, y muy
curioso por el software o informática, es tu deber, mostrarle esta
conexión como mínimo, para que, quizá algún día, genere bienestar a
todos, como el pequeño Tesla o Edison soñaron y alcanzaron.


Sus sueños son posibles nuevos pequeños tesla...


Y si el receptor no se convenció quizá sea bueno preguntarse:


¿y qué ganan los demás diciendo esto?, en resumen si no les parece
bien fomentar el bienestar en su entorno, quizá sus metas son
diferentes para conectar los cables, podría decirle que cuando
compartí software libre, se me retribuyo con alcances técnicos, que se
tradujo en aumento salarial y bases solidas para postular a un mejor
puesto de trabajo.
