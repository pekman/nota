# grep busqueda de palabras dentro de archivos


`grep -nr "palabraExacta" *`


^ esto listará los archivos


`grep -nr "palabraExtacta" * | grep ".php"`

^ esto listará los archivos que contentan la palabraExtacta escrita
dentro y que contengran en el listado la palabra exacta ".php"


`grep -B 3 -A 2 foo README.md`

^ -B para n líneas antes y -A para n líneas despues

`grep -C 3 foo README.md`

^ esto mostrará 3 líneas antes y 3 líneas después

`wmctrl -l | grep -iq "xpalabra"`

^ esto da resultado falso si no se encuentra la xpalabra ignorando
mayusculas o minusculas, nota: wmctrl "lista las ventanas en pantalla"
