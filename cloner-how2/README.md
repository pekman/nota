# Clonado de Hypercuevas v1.0

## Debes hacer un backup en tar.gz y deberías poder hacer doble clic y mirar dendro del backup

Así es como dice el enunciado, si haces este comando por ejemplo en
sda1, deberias porder ver en el resultado, las carpetas contenidas, de
no ser así, el archivo está dañado, no puedes arriesgarte a formatear
la partición origen.

## Ejemplo


    ssh remote-username@192.168.0.108 "dd if=/dev/sda1 | gzip -1 -" | dd of=path/to/local/backup-sda1.gz


- ^ trae la partición desde otra pc con resultante en gzip
- ^ deberías poder hacerle doble clic y ver el interior


# Old

## Hacia particiones de diferentes tamaños

### Tamaño de fuente debe ser menor al tamaño de destino

    dd if=/dev/sdc2 of=/dev/sda2 status=progress

### Rellenar el tamaño del disco

##### _Deberíamos hacer un backup iso, img, etc. del disco por si tenemos información importante_

Cuando haces un clonado hacia un disco duro o ssd, es común que el destino
sea mucho mas grande que la fuente de la clonacion, entonces usamos
los siguientes comandos, para corregir el espacio no usado del destino

- boot live hyperiso
- lsblk
- Identificar donde hicimos la clonacion
    * Explicacion: "En mi caso `/dev/sda2`"
    * Explicacion: "Como cloné desde un usb con 16G, sólo podemos usar 16G
      en la particion `/dev/sda2` de 40G"
- `df -h`
- `fsck -y /dev/sdaN`
- `fsck -fy /dev/sdaN`
- `e2fsck -f /dev/sdaN`
- `resize2fs /dev/sdaN`
- ^ remplace N por el nro de la partición
- ^ y a por la letra del disco en su caso


Al reiniciar ya veo que tengo mas espacio disponible en disco

nota: "Aveces el comando nos dice que ejecutemos algunos comandos
extra en mi caso los mismos comandos me indicaron estos pasos"

# Clonado de Hypercuevas v0.1

# Hacia particiones de diferentes tamaños

## Tamaño de raiz debe ser menor al tamaño de destino

Ejm. 16G(6,2G de raiz) -> hacia un disco de -> 7,2G

bs=1M count=7168 <- quiere deresultado 7resultaráen 7G

a pesar de que la fuente es de 16G

para copias hacia discos del mismo tamaño no se necesita especificar tamaño de salida

    dd if=/dev/sdc2 of=/dev/sdd2  bs=1M  count=7168 status=progress

## Clonado desde la imagen qcow2 dentro de qemu

    dd if=/dev/sda | gzip -1 - | ssh user@local dd of=image.gz

- ^ el resultado se vuelve a clonar en el disco o usb particion por particion

## Clone to smaller disk partition



- You need to shrink the partitions on the source first (or delete
  those out-of-bounds).

- Than dd and after you will probably need to repair the partition
  table by using


         gdisk /dev/sd<target>

- and the key sequence to repair the table is


        v r d w


- I suggest you to shring the partitions a bit smaller than needed and
  after expand them back to the full size of the target disk.


- (This answer is based on my personal experience when cloning my HDD
  to a smaller SSD)
