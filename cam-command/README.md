# Mostrar una cam elegante para las trasmiciones de pantalla


    mplayer tv://device=/dev/video01 -vf mirror


## cuando


- Aveces varia

- depende del comando `ls /dev/`

    * EJM

    `video01 -> video0`

# Screen Cast to /dev/video0 or fake WebCam

## Compilling

    git clone https://aur.archlinux.org/v4l2loopback-dkms-git.git
    cd v4l2loopback-dkms-git
    doas libremakepkg
    doas modprobe v4l2loopback

## Installing

    doas pacman -U *pkg*lz

## Uninstalling

If you want to remove the package you compiled:

`doas pacman -R v4l2loopback-dkms`

## Usage Examples

Note that the actual video number may vary depending if an existing
device is already using /dev/video0. Check output of ls /dev/video* or
v4l2-ctl --list-devices.

## Desktop to virtual camera

Now run ffmpeg. Example for desktop using x11grab:

    ffmpeg -f x11grab -framerate 15 -video_size 1280x720 -i :0.0 -f v4l2 /dev/video0

## Video file (MP4) to virtual camera

    ffmpeg -re -i input.mp4 -map 0:v -f v4l2 /dev/video0

## Image to virtual camera

    ffmpeg -re -loop 1 -i input.jpg -vf format=yuv420p -f v4l2 /dev/video0


_Webcam → ffmpeg → Virtual webcam_


## Add text

With drawtext filter:

    ffmpeg -f v4l2 -i /dev/video0 -vf "drawtext=text='Hello World':fontsize=12:fontcolor=white:font=Arial:x=w-tw-10:y=h-th-10,format=yuv420p" -f v4l2 /dev/video1

### See How to position drawtext text?

_Greenscreen / chroma key / replace background_

## Using chromakey, overlay, and format filters:

    ffmpeg -re -i background.jpg -f v4l2 -i /dev/video0 -filter_complex "[1]chromakey=color=#326964:similarity=0.07:blend=0.02[fg];[0][fg]overlay=(main_w-overlay_w)/2:(main_h-overlay_h)/2:format=auto,format=yuv420p" -f v4l2 /dev/video1

_See How to position overlay?_

### Preview with ffplay

    ffplay /dev/video0

## Common errors

    Unable to open V4L2 device '/dev/video0'
    Failed to open /dev/video0: No such file or directory
    Unknown V4L2 pixel format equivalent for yuvj422p

_Source: [see source](https://askubuntu.com/questions/881305/is-there-any-way-ffmpeg-send-video-to-dev-video0-on-ubuntu)_
