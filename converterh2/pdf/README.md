# PDF

## Convertir de PDF a DOC

### hacia DOC

soffice --infilter="writer_pdf_import" --convert-to doc nombre.pdf

### hacia ODT

soffice --infilter="writer_pdf_import" --convert-to doc nombre.pdf

## Convertir de PDF a DOCX

libreoffice --invisible --convert-to docx:"MS Word 2007 XML" nombre.pdf

## Usando Abiword a DOC

abiword --to=doc nombre.pdf

### PNG a PDF

gm convert *png nombre.pdf

    ^ order and size: compatible with scrot heckyel/i3-config keybinding
