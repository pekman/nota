# Solución Modular DevTech


## 1000 Alumnos


## Fullstack


## Precio de VPS + DOMINIO con (.info)


[x] tudominio.com/acceso (cokkies, etc)


[x] tudominio.com/modulos


## Apps


### Chat


|      URL modelo     |     Software     |       Imagen     | Precio Instalación |  Time aprox  |
| :-----------------: | :--------------: | ---------------- | :----------------: | :----------: |
| tudominio.com/chat  | https://github.com/conversejs/converse.js | [screenshot][i0] |    5 PEN / Hora    |   32 horas   |


### Consulta, Forum


|      URL modelo     |   Software   |      Imagen      | Precio Instalación |  Time aprox  |
| :-----------------: | :----------: | ---------------- | :----------------: | :----------: |
| tudominio.com/foro  | https://github.com/punbb/punbb  | [screenshot][i1] |    5 PEN / Hora    |   24 horas   |


### Cuestionario


|        URL modelo           |     Software     |       Imagen       | Precio Instalación |    Time     |
| :-------------------------: | :--------------: | :----------------: | :----------------: | :---------: |
| tudominio.com/cuestionario  | https://framagit.org/framasoft/framaforms  | [screenshot][i2]   |    5 PEN / Hora    |  24 horas   |


### Lección


|      URL modelo        |     Software     |      Imagen      | Precio Instalación |     Time    |
| :--------------------: | :--------------: | ---------------- | :----------------: | :---------: |
| tudominio.com/leccion  | https://github.com/jitsi/jitsi-meet"  | [screenshot][i3] |    5 PEN / Hora    |  24 horas   |</p>


### Agenda


|       URL modelo      |     Software     |       Imagen     | Precio Instalación |     Time    |
| :-------------------: | :--------------: | ---------------- | :----------------: | :---------: |
| tudominio.com/agenda  | https://nextcloud.com/   | [screenshot][i4] |    5 PEN / Hora    |  30 horas   |</p>


### Drive


|       URL modelo       |     Software     |       Imagen     | Precio Instalación |    Time     |
| :--------------------: | :--------------: | ---------------- | :----------------: | :---------: |
| tudominio.com/archivo  | https://nextcloud.com/   | [screenshot][i5] |    5 PEN / Hora    |  30 horas   |</p>


### Calendario


|         URL modelo        |     Software     |       Imagen     | Precio Instalación |    Time     |
| :-----------------------: | :--------------: | ---------------- | :----------------: | :---------: |
| tudominio.com/calendario  | https://framagit.org/shikaruko/framadate   | [screenshot][i6] |    5 PEN / Hora    |  24 horas   |


### Notas adhesivas


|      URL modelo      |     Software     |      Imagen      | Precio Instalación |    Time     |
| :------------------: | :--------------: | ---------------- | :----------------: | :---------: |
| tudominio.com/notas  | https://framagit.org/framasoft/framemo     | [screenshot][i7] |    5 PEN / Hora    |  24 horas   |


## Contáctanos


Consultas al desarrollador mailto:heckyel@hyperbola.info Jesús E.


072-281342


Requiere de datos
