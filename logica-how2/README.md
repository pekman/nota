# Simbolos Chvrs

#### ¿Quien dijo que no se podía estudiar sin cuaderno?

## x²
### Potencia

Gnu/linux tiene algunas similitudes con Windows cuando de escritura se
trata, sin embargo mantiene sus diferencias con el mencionado sistema
operativo, por lo que a continuación te mostraremos cómo sacar el
símbolo del al cuadrado en Gnu/Linux.

Con el teclado
Haciendo uso del acento circunflejo (^) que podrás encontrar a la
derecha de la letra P, podrás colocar la potencia a cuadrado a
cualquier número.

Si quieres colocar el número 5 al cuadrado, solo debes colocar el
símbolo ^ seguido por el número 2, es decir, 5^2.

Luego debes presionar la tecla enter y automáticamente el sistema
cambiará la ecuación por 5².

## √ ∛ ∜ ∝ ∞
### Raíz y Infinítos

Ctrl+Shift+u 221A √

Ctrl+Shift+u 221B ∛

Ctrl+Shift+u 221C ∜

Ctrl+Shift+u 221D ∝

Ctrl+Shift+u 221E ∞

Si el símbolo ^ también significa elevado a una potencia determinada,
por ejemplo: x al cuadrado = x^2, entonces, si consideras que la raiz
también se expresa en fracciones, (al cuadrado se eleva a 1/2, al cubo
se eleva a 1/3, a la cuarta se eleva a 1/4...), por ejemplo la raiz
cuadrada de x se expresa como x elevada a 1/2, entonces puedes
expresarla como x ^ (1/2).

raiz cuadrada de x = x ^(1/2)

raiz cúbica de x = x ^(1/3)

raíz cuarta de x = x ^(1/4)
