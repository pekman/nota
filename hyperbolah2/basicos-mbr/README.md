#!/bin/bash

pacman -S xenocara-video-vesa \
xenocara-server xenocara-xinit xenocara \
mesa mesa-demos \
ttf-liberation ttf-bitstream-vera ttf-dejavu ttf-droid \
pulseaudio pulseaudio-alsa alsa-utils pavucontrol \
slim \
gamin gvfs \
dhcpcd-ui \
gnome-kering \
volumeicon \
ntp \
zlib haskell-zlib \
bzip2 \
unar \
p7zip lrzip \
zip libzip unzip \
udevil autofs \
ntfs-3g fuse \
fatsort exfat-utils dosfstools \
xfsprogs \
ffmpeg gstreamer gst-libav gst-plugins-bad \
gst-plugins-good gst-plugins-ugly \
gst-plugins-base gstreamer-vaapi gst-transcoder \
ffms2 x264 libvorbis libvpx libtheora opus vorbis-tools \
mpv \
epdfview \
viewnior \
iceape-uxp iceape-uxp-l10n-es-es \
libreoffice-still libreoffice-still-l10n-es \
hunspell hunspell-es \
hyphen hyphen-es \
mythes mythes-es \
firejail firetools \
gajim python2-axolotl \
qtox \
toxic
