# GRUB

## Desde una imagen iso para instalar grub ↓ [ ]

`grub-install /dev/sda`

## Elegir la opcion al encender el grub

debemos editar el archivo de configuración del grub

`sudo emacs /etc/default/grub`

`sudo update-grub`

y reiniciamos

## Compilar grub.cfg

`sudo grub-mkconfig -o /boot/grub/grub.cfg`
