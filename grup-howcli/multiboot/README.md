# ¿Cómo instalar dos sistemas gnu linux en el mismo disco?

### Requerimientos

- `service lvm start`
- `pacman -S grub-bios`
- `pacman -S linux-libre-lts`
- `genfstab -U -p / >> /etc/fstab`
- `mkinitcpio -p linux-libre-lts`

## Hyperbola y Ubuntu


imprimo mi particionado con


`sudo cfdisk /dev/sda`


    Device       Boot    Size  Id Type
    /dev/sda1    *       500M  83 Linux
    /dev/sda2           44.3G  83 Linux
    /dev/sda3            417G   5 Extended
    ├─/dev/sda5         44.3G  83 Linux
    └─/dev/sda6        372.7G  83 Linux
    /dev/sda4              4G  82 Linux swap / Solaris



## imprimo el fstab del sistema anfitrion `/dev/sda2` Hyperbola


    # /dev/sda2
    UUID=f0279d1c-a86b-40b9-862b-cef123f9ef54	/         	ext4      	rw,relatime,data=ordered	0 1

    # /dev/sda6
    UUID=2faf7353-0228-43bd-95f4-b16d4fea1320	/home     	ext4      	rw,relatime,data=ordered	0 2

    # /dev/sda1
    UUID=597cf3fb-08e3-420a-8564-4669bd3d90a3	/boot     	ext4      	rw,relatime,stripe=4,data=ordered	0 2

    # /dev/sda4
    UUID=8e2a5d1b-0ee7-4ed4-ad08-85cf9ef3a3d1	none      	swap      	defaults  	0 0


## donde el sistema adicional está en `/dev/sda5` Ubuntu 18.04 con los siguientes datos


    Partition type: Linux (83)
    │Filesystem UUID: 57400df8-a05a-4850-b17c-30bdff523f5f
    │     Filesystem: ext4
    │     Mountpoint: / (mounted)


## Identificar Imágenes linuz ejemplo


    /boot
    ├── System.map-4.15.0-123-generic
    ├── backup-grub
    ├── config-4.15.0-123-generic
    ├── config-linux-libre-lts
    ├── grub
    ├── initrd.img-4.15.0-123-generic
    ├── lib
    ├── lost+found
    ├── vmlinuz-4.15.0-123-generic
    └── vmlinuz-linux-libre-lts


Debemos verificar que tengamos las imágenes tanto de hyperbola como de ubuntu


## Instrucción para bootear sistema adicional gnu/linux


agregar en `/etc/grub.d/40_custom` lo siguiente:


    #!/bin/sh
    exec tail -n +3 $0
    # This file provides an easy way to add custom menu entries.  Simply type the
    # menu entries you want to add after this comment.  Be careful not to change
    # the 'exec tail' line above.

    menuentry "Ubuntu 18.04" --class ubuntu --class os {
        load_video
        set gfxpayload=keep
        insmod gzio
        insmod part_msdos
        insmod ext2
        set root='hd0,msdos1'
        if [ x$feature_platform_search_hint = xy ]; then
          search --no-floppy --fs-uuid --set=root --hint-bios=hd0,msdos1 --hint-efi=hd0,msdos1 --hint-baremetal=ahci0,msdos1  597cf3fb-08e3-420a-8564-4669bd3d90a3
        else
          search --no-floppy --fs-uuid --set=root 597cf3fb-08e3-420a-8564-4669bd3d90a3
        fi
        search --no-floppy --set=root --fs-uuid 57400df8-a05a-4850-b17c-30bdff523f5f
        # chainloader /dev/sdb5
        echo	'Loading Ubuntu kernel ...'
        linux	/vmlinuz root=UUID=57400df8-a05a-4850-b17c-30bdff523f5f rw  quiet
        echo	'Cargando imagen de memoria inicial...'
        initrd	/boot/initrd.img-4.15.0-123-generic
    }


^ esto va a recorrer todas las imágenes (vmlinuz del `/boot`) y agregará un menú en el grub para ejecutarlo al ejecutar el siguiente comando


## Compilar los menu entries


el siguiente comando generará el ejecutable que la laptop usará para mostrar el menú grub

- si tenemos un menú grub pasado, por si las dudas hacemos un respaldo

`sudo mv /boot/grub /boot/backub-grub`

- ^ una vez corrido esto generamos uno nuevo (posterior mente podemos eliminar el backup-grub si todo funciona)

- `sudo grub-install --recheck --target=i386-pc /dev/sda`

- `sudo grub-mkconfig -o /boot/grub/grug.cfg`

- ^ este comando escribirá la fuente donde se está indicando y generará lo necesario para mostrar el menú grub al encender

## Notas

- Deberías comparar los UUID <-- para saber donde va todo en tu propio disco, el README está escrito con ejemplos reales
- Si deseas que arranque en hyperbola o ubuntu debes saber la posición en el menú y de ahí indicarla en `/etc/default/grub` en el apartado GRUB_DEFAULT=0 <-- que para este ejemplo arranque en Hyperbola, en el sistema anfitrion Hyperbola
- tambien necesitas indicar la opcion lvm en `/etc/default/grub` que es la siguiente --> `GRUB_PRELOAD_MODULES=lvm`, en el sistema anfitrion Hyperbola
