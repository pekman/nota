# Binear

- es un atajo personal como un archivo en una carpeta bin.

- Ejemplo 1:

        #!/bin/bash

        emacs --eval="(neotree-dir \"../personal/$1\")"

- Ejemplo 2:

        #!/bin/bash

        siCorriendo=$( pidof emacs )              \
            ||                                    \
            emacs --eval="(dired \"../gema/$1\")" \
                &&                                \
                emacs --eval="(progn (set-background-color \"#000009\") (dired \"../gema/$1\"))"

- ^ y se puede poner en /usr/bin/personal

- será reconocido como un comando nuevo
