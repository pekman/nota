# HYPERBOLA

ip a

wpa_supplicant -B -i wlp0s20f0u4 -c <(wpa_passphrase "ROBISTAR_8EI9" "PasswordX2021")

# TRISQUEL

nmcli d wifi connect "ROBISTAR_8EI9" password "PasswordX2021"

# Conectar la wifi desde tty

## Detectar wifi

    iw dev

## Activar dispositivo de Red

    ip link set <nombre-del-dispositivo> up

## Activar Internet con wpa_supplicant

    wpa_supplicant -B -i <nombre-de-dispositivo> -c <(wpa_passphrase "ssid" "psk")

## Renovar IPv4 con dhcpcd

    dhcpcd <nombre-de-dispositivo>
