# Auto login TTY

- Create a file `/etc/conf.d/agetty-autologin`:


- Type in the file:


`agetty_options="--autologin <username> --noclear"`


- ^ replace `<username>` by autologin user: example `saravia`


- Replace the agetty.tty1 service with one called called agetty-autologin.tty1:


`cd /etc/init.d`


`sudo rc-update del agetty.tty2`


`sudo mv agetty.tty2 agetty-autologin.tty2`


`sudo rc-update add agetty-autologin.tty2 default`
