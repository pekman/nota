# ls (L)ist directorie content(S)


## Notas personales


### listar sólo archivos ocultos


- En sistemas gnu/linux para ocultar un archivo sólo hace falta
  renombrarlo con un `.` al inicio. Para listar los archivos ocultos
  se usa el parámetro `-a` así cómo para sólo listar el contenido del
  folder actual se usa el parámetro `-d`. la tubería nos permite
  adicionar una especie de comando atributo, que modificaría la salida
  como por ejemplo `| grep palabra` en este caso listaría sólo las
  líneas que contengan los caracteres `palabra`. grep un comando muy
  útil como la mayoría de los comandos, tiene un atributo genial `-v`
  el cual ignoraría todas las líneas que contengan los caracteres
  `palabra`.


  ^ para este análisis podemos ver que atributo es parecido a decir
  opción. entonces el comando sería así:


  `ls -ad .* | grep -v /`


  ^ en donde podemos ver que `.*` nos dice: todos los archivos que
  comienzen con el caracter `.`
