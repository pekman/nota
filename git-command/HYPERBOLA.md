## were <user> its your username eg: quiliro

######

# for the remote git management you have these two commands:

ssh -p 51100 team@git.hyperbola.info   # the team repo
ssh -p 51100 <user>@git.hyperbola.info   # your repo

# with ssh to team or your repo you can only use 4 commands
# "list" is for listing repositories
# 'description' for adding a description or a title to that repo
# 'make' to create a repository
# 'remove' to remove
# type the command make for example
# wen it says Please specify the address of the Git repository:
# its asking for the folder name
# for example "documentation" is the folder for many repositories
# like guide.git were i have uploaded my file
# then it prints Please specify the name of the Git repository:
# you cant type: "guide"
# to create "documentation/guide.git" in the remote repo team or <user>

######

# in your local machine Create folders as follow:
# open your terminal and type:

mkdir GIT && cd GIT && mkdir team && cd team && mkdir documentation && mkdir hyperbolabsd && mkdir packages && mkdir services && mkdir software

# to end up creating these folders
# GIT/team/documentation
# GIT/team/hyperbolabsd
# GIT/team/packages
# GIT/team/services
# GIT/team/software

# now cd to team/packages:

cd packages

#[x@localhost packages]$
# Clone the repositories:

git clone ssh://<user>@git.hyperbola.info:51100/~git/~team/packages/core.git
git clone ssh://<user>@git.hyperbola.info:51100/~git/~team/packages/extra.git

# then cd to the extra repository:

cd extra

# To create your own branch in team git:

git branch <user>   # example "git branch quiliro" to create a new branch
git checkout <user>   # to switch to your branch

# create the files you want to add to extra repository and type:

git add file1 file2
git commit -m "Add your commit comment here"
git commit -a -m "Add your commit comment here"  #this is used to add all commits

# to push and pull from the team repository make these changes inside team/packages/extra/.git/config
# change <user> to your username:

[core]
	repositoryformatversion = 0
	filemode = true
	bare = false
	logallrefupdates = true
[remote "origin"]
	url = ssh://<user>@git.hyperbola.info:51100/~git/~team/packages/extra.git
	fetch = +refs/heads/*:refs/remotes/origin/*
        pushurl = ssh://team@git.hyperbola.info:51100/~git/~team/packages/extra.git
[branch "master"]
	remote = origin
	merge = refs/heads/master
[branch "milky-way-v0.4"]
	remote = origin
	merge = refs/heads/milky-way-v0.4
[branch "<user>"]
	remote = origin
	merge = refs/heads/<user>

# to push and pull commits type

git push origin <user>   # to push commits to your branch
git pull origin master   # pull commits from the master branch to your branch

# to merge commits to the master branch

git checkout master
git merge <user>

# then you can push to remote master branch

git push origin master   # to push commits to the master branch

## we countinue with more git commands

git reset --soft HEAD~1   # reverts locally your commits ... however it keeps your modifications
git reset --hard HEAD~1   # reverts and removes your latest commit change
git reset --hard HEAD~2   # if you have plans to remove more commits (2 in this case), if you didn't push those commits remotely
git format-patch -1   # generates a patch of the latest commit
git revert $commit_checksum   # the best way to avoid piss off the work of the other members

# If you would revert this commit -> https://git.hyperbola.info:50100/packages/extra.git/commit/?id=85b468a2c03371c4e3550a0ddc529a9df376e6ae
# you should run:

git revert 85b468a2c03371c4e3550a0ddc529a9df376e6ae   # It will generate a new commit by reverting your commit.

# If it comes from your own branch (no master please!) you could run:

git reset --hard HEAD~1   # (for the latest commit) or:
git reset --hard HEAD~2   # (for the latest 2 commits), then you could run:
git push --force

# However, please don't use this command in master, for that reason i'm insisting for each user to create their own branches.

git checkout master   # to pull changes from your branch to master, then:
git pull --rebase origin <user>
git push origin master && git push origin <user>   # to push them (in your case)

# Remember to push those commits to master and milky-way-v0.4 branches if you consider those ones stable and tested by you first because I will pull them to our build server to build and push to repos.
# You have your own user repo to make experimental things there.  You could use forced commands such as:

git push --force   # because it is in fact your own personal repo for experimental reasons.

# So, in short, you have 2 ways to push your commits:
# 1) your own repo for experimental reasons and then,
# 2) branches for testing
