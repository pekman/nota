# GIT

## Ignorar literalmente solo en local

En la carpeta del proyecto git (este es un ejemplo con los GTAGS autogenerados de emacs)
- No necesitamos subir estos archivos al repo entonces...

        echo "slcore/GPATH" >> .git/info/exclude
        echo "slcore/GTAGS" >> .git/info/exclude
        echo "slcore/GRTAGS" >> .git/info/exclude

## Para dejar de seguir archivos [*]

### dejar de seguir

`git update-index --assume-unchanged path/to/file`

### para seguir nuevamente

`git update-index --no-assume-unchanged path/to/file`

## Para ver cambios pero sin espacios

`git diff --ignore-space-at-eol -b -w --ignore-blank-lines [commit]`

## Ejemplo de .gitconfig genial

    [user]
	    email = saravia

    [color "diff"]
	    meta = yellow bold
	    frag = magenta bold
	    old = red bold
	    new = green bold

    [core]
	    editor = emacs

    [alias]
	    superlog = log --graph --abbrev-commit --decorate --date=relative --format=format:'%C(bold blue)%h%C(reset) - %C(cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all
	    olvidarse = update-index --assume-unchanged
	    acordarse = update-index --no-assume-unchanged

## Cómo corregir los _^M_ en el git diff -w

`git config core.whitespace cr-at-eol`

## Hacer un import

ejemplo:

`curl "https://libregit.org/heckyel/libretube/commit/12445bca67c758cf8cec837559ba7073baf02e35.patch" -o 0001.patch`
`git apply --index 0001.patch`

## Deshacer commits

ejemplo:

- Deshacer el ultimo commit por sí indicamos mal el commit

        git reset --soft HEAD~1

- ^ y si queremos agregar un cambio a lo que commiteamos por si acabamos de acordarnos

        git reset HEAD path/to/archive/donde/nos/falta/corregit.txt

## Opcional

ejemplo:

- Este falta confirmarlo, pero segun parece si hicimos varios commits
  podemos deshacer los commits hasta el SHA-1 "nota si nó enviamos al
  repositorio los cambios tenemos el control con estos comandos"

        git reset --soft 44012507

- Aquí tocaria un

        git reset HEAD .

- ^ eso para deshacer el git add -A

## Hacer un parche

- La siguiente línea, hace un parche del último y penúltimo commit
  hecho, no necesariamente publicado en producción.

- La primera barbulilla del último commit y la segunda del
  antepenúltimo commit

        git format-patch HEAD~~
